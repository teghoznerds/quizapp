using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using Quizapp.Models;

namespace Quizapp.Areas.Admin.Pages
{
    public class AddSchoolModel : QuizappAdminPageBase
    {       
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private QuizappContext _context;
        private readonly ApplicationSettings _applicationSettings;
        private IHttpContextAccessor _accessor;

        public AddSchoolModel(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            QuizappContext context,
            RoleManager<IdentityRole> roleManager,
            IOptions<ApplicationSettings> options,
            IHttpContextAccessor accessor) : base(userManager, signInManager, context, roleManager, options)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _context = context;
            _applicationSettings = options.Value;
            _accessor = accessor;
        }
        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                //return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
                return RedirectToPage("Login");
            }
            UserInfo = user;
            Admin = user.Admin ?? _context.tblAdmin.Find(user.AdminUserId);
            ListOfSchools = _context.tblSchool.ToList();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Models.School input, string returnUrl = null)
        {
            returnUrl = Url.Content("~/Admin/Schools");

            if(_context.tblSchool.Any(s => s.Name == input.Name && s.Username == input.Username) == false)
            {
                _context.tblSchool.Add(input);
                int i = _context.SaveChanges();

                if (i > 0)
                {
                    ApplicationUser user = new ApplicationUser
                    {
                        SchoolId = input.Id,
                        Email = input.Email,
                        UserName = input.Username,
                        SchoolName = input.Name,
                        PhoneNumber = input.PhoneNumber,
                        EmailConfirmed = true,
                    };

                    var result = await _userManager.CreateAsync(user, input.Password);
                    if (result.Succeeded)
                    {
                        await _userManager.AddToRoleAsync(user, "School");

                        var message = new ValidationMessage();
                        message.Type = ValidationMessageType.Success;
                        message.Message = $@"{input.Name} School Added";
                        MessageStr = message.SerializeMessage();
                        return LocalRedirect(returnUrl);
                    }
                }
                else
                {
                    var message = new ValidationMessage();
                    message.Type = ValidationMessageType.Success;
                    message.Message = $@"{input.Name} School or {input.Username} Username Allready Exist";
                    MessageStr = message.SerializeMessage();
                }
            }
            

            return Page();
        }

        public List<Models.School> ListOfSchools { get; set; }
        [BindProperty]
        public Models.School Input { get; set; }

    }
  
}