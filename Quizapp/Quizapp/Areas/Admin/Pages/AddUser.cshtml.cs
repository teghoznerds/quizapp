using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using Quizapp.Models;

namespace Quizapp.Areas.Admin.Pages
{
    public class AddUserModel : QuizappAdminPageBase
    {       
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private QuizappContext _context;
        private readonly ApplicationSettings _applicationSettings;
        private IHttpContextAccessor _accessor;

        public AddUserModel(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            QuizappContext context,
            RoleManager<IdentityRole> roleManager,
            IOptions<ApplicationSettings> options,
            IHttpContextAccessor accessor) : base(userManager, signInManager, context, roleManager, options)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _context = context;
            _applicationSettings = options.Value;
            _accessor = accessor;
        }
        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                //return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
                return RedirectToPage("Login");
            }
            UserInfo = user;
            Admin = user.Admin ?? _context.tblAdmin.Find(user.AdminUserId);
            UserRoles = _context.Roles.Where(c => c.NormalizedName != "STUDENT" && c.NormalizedName != "SCHOOL").ToList();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Models.Admin input, string returnUrl = null)
        {
            returnUrl = Url.Content("~/Admin/Users");
            _context.tblAdmin.Add(input);
            _context.SaveChanges();

            if (input.Id != 0)
            {
                var user = new ApplicationUser
                {
                    UserName = Input.PhoneNumber,
                    Email = Input.Email,
                    AdminUserId = input.Id,
                    FirstName = input.FirstName,
                    LastName = input.LastName,
                    PhoneNumber = input.PhoneNumber,
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true
                };

                var result = await _userManager.CreateAsync(user, input.DefaultPassword);
                if (result.Succeeded)
                {
                    foreach (var role in _context.Roles.Where(c => c.NormalizedName != "MEMBER" && c.NormalizedName != "ADMINSUPER").ToList())
                    {
                        var element = $@"R-{role.Name}";
                        if (Request.Form[element] == "on")
                        {
                            await _userManager.AddToRoleAsync(user, role.Name);
                        }
                    }
                    await _userManager.AddToRoleAsync(user, "Admin");

                    return LocalRedirect(returnUrl);
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "User already exist");
            }
            return Page();
        }

        [BindProperty]
        public Models.Admin Input { get; set; }
        public List<IdentityRole> UserRoles { get; set; }

    }
  
}