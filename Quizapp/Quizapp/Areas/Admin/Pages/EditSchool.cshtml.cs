using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using Quizapp.Models;

namespace Quizapp.Areas.Admin.Pages
{
    public class EditSchoolModel : QuizappAdminPageBase
    {       
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private QuizappContext _context;
        private readonly ApplicationSettings _applicationSettings;
        private IHttpContextAccessor _accessor;

        public EditSchoolModel(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            QuizappContext context,
            RoleManager<IdentityRole> roleManager,
            IOptions<ApplicationSettings> options,
            IHttpContextAccessor accessor) : base(userManager, signInManager, context, roleManager, options)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _context = context;
            _applicationSettings = options.Value;
            _accessor = accessor;
        }
        public async Task<IActionResult> OnGetAsync(int id)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                //return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
                return RedirectToPage("Login");
            }
            UserInfo = user;
            Admin = user.Admin ?? _context.tblAdmin.Find(user.AdminUserId);
            SelectedSchool = _context.tblSchool.Where(s => s.Id == id).FirstOrDefault();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Models.School input, string returnUrl = null)
        {
            returnUrl = Url.Content("~/Schools");
            _context.Entry(Input).State = Microsoft.EntityFrameworkCore.EntityState.Detached;
            _context.Entry(Input).State = Microsoft.EntityFrameworkCore.EntityState.Detached;
            _context.Entry(Input).State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            ApplicationUser user = _context.Users.Where(u => u.SchoolId == Input.Id).FirstOrDefault();
            user.SchoolName = input.Name;
            user.PhoneNumber = input.PhoneNumber;
            user.Email = input.Email;
            user.UserName = input.Username;
            await _userManager.UpdateAsync(user);

            return RedirectToPage(returnUrl);
        }
        [BindProperty]
        public Models.School Input { get; set; }
        public Models.School SelectedSchool { get; set; }

    }
  
}