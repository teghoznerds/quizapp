using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using Quizapp.Models;

namespace Quizapp.Areas.Student.Pages
{
    public class AnswerQuestionsModel : QuizappStudentPageBase
    {       
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private QuizappContext _context;
        private readonly ApplicationSettings _applicationSettings;
        private IHttpContextAccessor _accessor;

        public AnswerQuestionsModel(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            QuizappContext context,
            RoleManager<IdentityRole> roleManager,
            IOptions<ApplicationSettings> options,
            IHttpContextAccessor accessor) : base(userManager, signInManager, context, roleManager, options)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _context = context;
            _applicationSettings = options.Value;
            _accessor = accessor;
        }
        public async Task<IActionResult> OnGetAsync(int quizId)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                //return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
                return RedirectToPage("Login");
            }
            UserInfo = user;
            Quiz = _context.tblTopicQuizzes.Where(q => q.Id == quizId).FirstOrDefault();
            QuizQuestions = _context.tblQuizzesQuestions.Where(q => q.TopicQuizzesId == quizId).ToList();
            QuizQuestionsChoices = _context.tblQuizzesQuestionChoices.Where(q => q.QuizzesQuestions.TopicQuizzes.Id == quizId).ToList();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Models.SchoolProgrammeStudents input, string returnUrl = null)
        {
            returnUrl = Url.Content("~/School/StudentProgrammes");
            _context.tblSchoolProgrammeStudents.Add(input);
            int i = _context.SaveChanges();
            if (i > 0)
            {
                var message = new ValidationMessage();
                message.Type = ValidationMessageType.Success;
                message.Message = $@"Program/Student Added";
                MessageStr = message.SerializeMessage();
                return LocalRedirect(returnUrl);
            }
            return Page();
        }
        [BindProperty]
        public Models.SchoolProgrammeStudents Input { get; set; }
        public Models.TopicQuizzes Quiz { get; set; }
        public List<Models.QuizzesQuestions> QuizQuestions { get; set; }
        public List<Models.QuizzesQuestionChoices> QuizQuestionsChoices { get; set; }


    }
  
}