using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Quizapp.Models;

namespace Quizapp.Areas.Student.Pages
{
    public class LoginModel : PageModel
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<LoginModel> _logger;
        private readonly QuizappContext _context;

        public LoginModel(SignInManager<ApplicationUser> signInManager, ILogger<LoginModel> logger, QuizappContext context, UserManager<ApplicationUser> userManager)
        {
            _signInManager = signInManager;
            _logger = logger;
            _context = context;
            _userManager = userManager;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string MessageStr { get; set; }
        public string ErrorMessage { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/Member");
            var Message = new ValidationMessage();

            try
            {
                if (ModelState.IsValid)
                {
                    // This doesn't count login failures towards account lockout
                    // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                    var result = await _signInManager.PasswordSignInAsync(Input.Username, Input.Password, Input.RememberMe, lockoutOnFailure: true);
                    if (result.Succeeded)
                    {
                        _logger.LogInformation("User logged in.");
                        var currentMember = _userManager.FindByNameAsync(Input.Username).Result;
                        var currentMemberId = currentMember.AdminUserId;

                        if (_userManager.IsInRoleAsync(currentMember, "Student").Result)
                        {
                            returnUrl = Url.Content("~/Student");
                        }
                        else
                        {
                            Message = new ValidationMessage
                            {
                                Type = ValidationMessageType.Danger,
                                Message = "You dont have a member role"
                            };
                            MessageStr = Message.SerializeMessage();

                            await _signInManager.SignOutAsync();
                            returnUrl = Url.Content("~/School");
                        }

                        return LocalRedirect(returnUrl);
                    }
                    if (result.RequiresTwoFactor)
                    {
                        return RedirectToPage("./LoginWith2fa", new { ReturnUrl = returnUrl, RememberMe = Input.RememberMe });
                    }
                    if (result.IsLockedOut)
                    {
                        _logger.LogWarning("User account locked out.");
                        return RedirectToPage("./Lockout");
                    }
                    else
                    {
                        Message.Type = ValidationMessageType.Danger;
                        Message.Message = "Invalid login attempt";
                        MessageStr = Message.SerializeMessage();

                        ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                        return Page();
                    }
                }
            }
            catch (Exception ex)
            {
                Message.Type = ValidationMessageType.Warning;
                Message.Message = ex.RecursiveMessages();

                //returnUrl = Url.Content("~/Member");
            }


            Message.Type = ValidationMessageType.Warning;
            Message.Message = "Please try again";
            MessageStr = Message.SerializeMessage();

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
  
}