using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Quizapp.Models;

namespace Quizapp.Areas.Student.Pages
{
    public class IndexModel : QuizappStudentPageBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private QuizappContext _context;
        private readonly ApplicationSettings _applicationSettings;
        private IHttpContextAccessor _accessor;

        public IndexModel(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            QuizappContext context,
            RoleManager<IdentityRole> roleManager,
            IOptions<ApplicationSettings> options,
            IHttpContextAccessor accessor) : base(userManager, signInManager, context, roleManager, options)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _context = context;
            _applicationSettings = options.Value;
            _accessor = accessor;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                //return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
                return RedirectToPage("Login");
            }
            UserInfo = user;
            Student = user.Student ?? _context.tblStudent.Find(user.StudentId);
            School = Student.School;
            List<int> studentProgrammes = new List<int>();
            studentProgrammes = Programs.Select(c => c.ProgrammeId.Value).ToList();
            ProgrammeCoursesList = _context.tblProgrammeCourses.Where(pc => studentProgrammes.Contains(pc.ProgramId)).ToList();           
            SyllabusList = _context.tblCourseSyllabus.Include(r => r.ProgrammeCourses).Include(r => r.Faculty)
                .Where(s => studentProgrammes.Contains(s.ProgrammeCourses.SchoolProgrammes.Id)).ToList();
            var syllabusIdList = SyllabusList.Select(sl => sl.Id).ToList();
            QuizsList = _context.tblTopicQuizzes.Include(q => q.SyllabusTopics).Where(q => syllabusIdList.Contains(q.SyllabusTopics.SyllabusId)).ToList();
            return Page();
        }

        public List<CourseSyllabus> SyllabusList { get; set; }
        public List<ProgrammeCourses> ProgrammeCoursesList { get; set; }
        public List<TopicQuizzes> QuizsList { get; set; }
        //public List<TopicQuizzes> QuizsList { get; set; }
    }
}