using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using Quizapp.Models;

namespace Quizapp.Areas.School.Pages
{
    public class EditStudentModel : QuizappSchoolPageBase
    {       
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private QuizappContext _context;
        private readonly ApplicationSettings _applicationSettings;
        private IHttpContextAccessor _accessor;

        public EditStudentModel(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            QuizappContext context,
            RoleManager<IdentityRole> roleManager,
            IOptions<ApplicationSettings> options,
            IHttpContextAccessor accessor) : base(userManager, signInManager, context, roleManager, options)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _context = context;
            _applicationSettings = options.Value;
            _accessor = accessor;
        }
        public async Task<IActionResult> OnGetAsync(int id)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                //return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
                return RedirectToPage("Login");
            }
            UserInfo = user;
            School = user.School ?? _context.tblSchool.Find(user.SchoolId);
            Input = _context.tblStudent.Where(s => s.Id == id).FirstOrDefault();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Models.Student input, string returnUrl = null)
        {
            returnUrl = Url.Content("~/School/Students");
            if(_context.tblStudent.Any(a => a.Email == input.Email) == false)
            {
                input.FullName = $@"{input.FirstName} {input.LastName}";
                int i = _context.SaveChanges();

                if (i > 0)
                {
                }
            }
            
            return Page();
        }

        [BindProperty]
        public Models.Student Input { get; set; }

    }
  
}