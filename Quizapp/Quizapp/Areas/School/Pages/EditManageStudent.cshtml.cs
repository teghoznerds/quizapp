using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using Quizapp.Models;

namespace Quizapp.Areas.School.Pages
{
    public class EditManageStudentModel : QuizappSchoolPageBase
    {       
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private QuizappContext _context;
        private readonly ApplicationSettings _applicationSettings;
        private IHttpContextAccessor _accessor;

        public EditManageStudentModel(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            QuizappContext context,
            RoleManager<IdentityRole> roleManager,
            IOptions<ApplicationSettings> options,
            IHttpContextAccessor accessor) : base(userManager, signInManager, context, roleManager, options)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _context = context;
            _applicationSettings = options.Value;
            _accessor = accessor;
        }
        public async Task<IActionResult> OnGetAsync(int id, int? studentId, int? programmeId)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                //return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
                return RedirectToPage("Login");
            }
            UserInfo = user;
            if (studentId.HasValue)
            {
                Student = _context.tblStudent.Where(s => s.Id == studentId.Value).FirstOrDefault();
            }

            if (programmeId.HasValue)
            {
                SchoolProgrammes = _context.tblSchoolProgrammes.Where(s => s.Id == programmeId.Value).FirstOrDefault();
            }

            School = user.School ?? _context.tblSchool.Find(user.SchoolId);
            ProgramSelectList = _context.tblSchoolProgrammes.Where(c => c.SchoolId == School.Id).ToList()
                .Select(c => new SelectListItem() { Text = $@"{c.Name}", Value = c.Id.ToString() }).ToList();

            StudentSelectList = _context.tblStudent.Where(c => c.SchoolId == School.Id).ToList()
                .Select(c => new SelectListItem() { Text = $@"{c.FirstName}", Value = c.Id.ToString() }).ToList();

            Input = _context.tblSchoolProgrammeStudents.Where(p => p.Id == id).FirstOrDefault();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Models.SchoolProgrammeStudents input, string returnUrl = null)
        {
            returnUrl = Url.Content("~/School/StudentProgrammes");
            _context.tblSchoolProgrammeStudents.Add(input);
            int i = _context.SaveChanges();
            if (i > 0)
            {
                var message = new ValidationMessage();
                message.Type = ValidationMessageType.Success;
                message.Message = $@"Program/Student Added";
                MessageStr = message.SerializeMessage();
                return LocalRedirect(returnUrl);
            }
            return Page();
        }
        public Models.Student Student { get; set; }
        public Models.SchoolProgrammes SchoolProgrammes { get; set; }
        [BindProperty]
        public Models.SchoolProgrammeStudents Input { get; set; }
        public List<SelectListItem> ProgramSelectList { get; set; }
        public List<SelectListItem> StudentSelectList { get; set; }

    }
  
}