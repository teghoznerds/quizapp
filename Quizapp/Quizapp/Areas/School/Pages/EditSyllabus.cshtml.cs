using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using Quizapp.Models;

namespace Quizapp.Areas.School.Pages
{
    public class EditSyllabusModel : QuizappSchoolPageBase
    {       
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private QuizappContext _context;
        private readonly ApplicationSettings _applicationSettings;
        private IHttpContextAccessor _accessor;

        public EditSyllabusModel(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            QuizappContext context,
            RoleManager<IdentityRole> roleManager,
            IOptions<ApplicationSettings> options,
            IHttpContextAccessor accessor) : base(userManager, signInManager, context, roleManager, options)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _context = context;
            _applicationSettings = options.Value;
            _accessor = accessor;
        }
        public async Task<IActionResult> OnGetAsync(int id)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                //return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
                return RedirectToPage("Login");
            }
            UserInfo = user;
            School = user.School ?? _context.tblSchool.Find(user.SchoolId);
            CoursesSelectList = _context.tblProgrammeCourses.Where(c => c.SchoolProgrammes.SchoolId == School.Id).ToList()
                .Select(c => new SelectListItem() { Text = $@"{c.Name}", Value = c.Id.ToString() }).ToList();
            FacultySelectList = _context.tblFaculty.Where(c => c.SchoolId == School.Id).ToList()
                .Select(c => new SelectListItem() { Text = $@"{c.FullName}", Value = c.Id.ToString() }).ToList();
            Input = _context.tblCourseSyllabus.Where(c => c.Id == id).FirstOrDefault();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Models.CourseSyllabus input, string returnUrl = null)
        {
            returnUrl = Url.Content("~/School/Syllabus");
            _context.tblCourseSyllabus.Add(input);
            int i = _context.SaveChanges();
            if(i > 0)
            {
                var message = new ValidationMessage();
                message.Type = ValidationMessageType.Success;
                message.Message = $@"{input.Name} Added";
                MessageStr = message.SerializeMessage();
                return Redirect(returnUrl);
            }
            return Page();
        }

        [BindProperty]
        public Models.CourseSyllabus Input { get; set; }
        public List<SelectListItem> CoursesSelectList { get; set; }
        public List<SelectListItem> FacultySelectList { get; set; }

    }
  
}