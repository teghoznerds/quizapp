using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using Quizapp.Models;

namespace Quizapp.Areas.School.Pages
{
    public class EditQuizQuestionAnswersModel : QuizappSchoolPageBase
    {       
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private QuizappContext _context;
        private readonly ApplicationSettings _applicationSettings;
        private IHttpContextAccessor _accessor;

        public EditQuizQuestionAnswersModel(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            QuizappContext context,
            RoleManager<IdentityRole> roleManager,
            IOptions<ApplicationSettings> options,
            IHttpContextAccessor accessor) : base(userManager, signInManager, context, roleManager, options)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _context = context;
            _applicationSettings = options.Value;
            _accessor = accessor;
        }
        public async Task<IActionResult> OnGetAsync(int? quizId)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                //return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
                return RedirectToPage("Login");
            }
            UserInfo = user;
            School = user.School ?? _context.tblSchool.Find(user.SchoolId);
            TopicSelectList = _context.tblSyllabusTopics.Where(c => c.CourseSyllabus.ProgrammeCourses.SchoolProgrammes.SchoolId == School.Id).ToList()
                .Select(c => new SelectListItem() { Text = $@"{c.Name}", Value = c.Id.ToString() }).ToList();
            if (quizId.HasValue)
            {
                Quiz = _context.tblTopicQuizzes.Where(q => q.Id == quizId).FirstOrDefault();
            }
            else
            {
                Quiz = null;
            }
            
            QuizQuestionTypes = Enum.GetValues(typeof(QuestionTypes)).Cast<QuestionTypes>().Select(c => new SelectListItem() { Text = $@"{c.ToString()}", Value = c.ToString() }).ToList();
            Quizzes = _context.tblTopicQuizzes.Where(c => c.SyllabusTopics.CourseSyllabus.ProgrammeCourses.SchoolProgrammes.SchoolId == School.Id).ToList()
                .Select(c => new SelectListItem() { Text = $@"{c.Name}", Value = c.Id.ToString() }).ToList();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Models.QuizzesQuestions input, string returnUrl = null)
        {
            returnUrl = Url.Content("~/School/QuizQuestions");
            _context.tblQuizzesQuestions.Add(input);
            int i = _context.SaveChanges();
            if (i > 0)
            {
                var message = new ValidationMessage();
                message.Type = ValidationMessageType.Success;
                message.Message = $@"{input.Questions} Added";
                MessageStr = message.SerializeMessage();
                return LocalRedirect(returnUrl);
            }
            return Page();
        }

        [BindProperty]
        public Models.QuizzesQuestions Input { get; set; }
        public TopicQuizzes Quiz { get; set; }
        public List<SelectListItem> TopicSelectList { get; set; }
        public List<SelectListItem> QuizQuestionTypes { get; set; }
        public List<SelectListItem> Quizzes { get; set; }

    }
  
}