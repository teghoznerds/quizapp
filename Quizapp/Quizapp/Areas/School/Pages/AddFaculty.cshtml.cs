using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using Quizapp.Models;

namespace Quizapp.Areas.School.Pages
{
    public class AddFacultyModel : QuizappSchoolPageBase
    {       
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private QuizappContext _context;
        private readonly ApplicationSettings _applicationSettings;
        private IHttpContextAccessor _accessor;

        public AddFacultyModel(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            QuizappContext context,
            RoleManager<IdentityRole> roleManager,
            IOptions<ApplicationSettings> options,
            IHttpContextAccessor accessor) : base(userManager, signInManager, context, roleManager, options)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _context = context;
            _applicationSettings = options.Value;
            _accessor = accessor;
        }
        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                //return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
                return RedirectToPage("Login");
            }
            UserInfo = user;
            School = user.School ?? _context.tblSchool.Find(user.SchoolId);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Models.Faculty input, string returnUrl = null)
        {
            returnUrl = Url.Content("~/School/Faculties");
            input.FullName = $@"{input.FirstName} {input.LastName}";
            _context.tblFaculty.Add(input);
            int i = _context.SaveChanges();

            if(i > 0)
            {
                ApplicationUser user = new ApplicationUser
                {
                    FirstName = input.FirstName,
                    LastName = input.LastName,
                    Email = input.Email,
                    UserName = input.Username,
                    PhoneNumber = input.PhoneNumber,
                    EmailConfirmed = true,
                };

                var result = await _userManager.CreateAsync(user, input.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, "SchoolFaculty");

                    var message = new ValidationMessage();
                    message.Type = ValidationMessageType.Success;
                    message.Message = $@"{input.FullName} Added";
                    MessageStr = message.SerializeMessage();
                    return LocalRedirect(returnUrl);
                }
            }

            return Page();
        }

        [BindProperty]
        public Models.Faculty Input { get; set; }

    }
  
}