﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class SchoolStudentRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "tblStudent",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 7, 15, 7, 33, 530, DateTimeKind.Local), new DateTime(2019, 3, 7, 15, 7, 33, 530, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 7, 15, 7, 33, 530, DateTimeKind.Local), new DateTime(2019, 3, 7, 15, 7, 33, 530, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 7, 15, 7, 33, 530, DateTimeKind.Local), new DateTime(2019, 3, 7, 15, 7, 33, 530, DateTimeKind.Local) });

            migrationBuilder.CreateIndex(
                name: "IX_tblStudent_SchoolId",
                table: "tblStudent",
                column: "SchoolId");

            migrationBuilder.AddForeignKey(
                name: "FK_tblStudent_tblSchool_SchoolId",
                table: "tblStudent",
                column: "SchoolId",
                principalTable: "tblSchool",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tblStudent_tblSchool_SchoolId",
                table: "tblStudent");

            migrationBuilder.DropIndex(
                name: "IX_tblStudent_SchoolId",
                table: "tblStudent");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "tblStudent");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 4, 22, 24, 42, 285, DateTimeKind.Local), new DateTime(2019, 3, 4, 22, 24, 42, 285, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 4, 22, 24, 42, 285, DateTimeKind.Local), new DateTime(2019, 3, 4, 22, 24, 42, 285, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 4, 22, 24, 42, 285, DateTimeKind.Local), new DateTime(2019, 3, 4, 22, 24, 42, 285, DateTimeKind.Local) });
        }
    }
}
