﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class TableSchemaUpdates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Duration",
                table: "tblSyllabusTopics",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "tblSchoolProgrammes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Duration",
                table: "tblSchoolProgrammes",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ProgramCoordinatorId",
                table: "tblSchoolProgrammes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "tblProgrammeCourses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Credit",
                table: "tblProgrammeCourses",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Duration",
                table: "tblProgrammeCourses",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "tblFacultyCourses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastUpdated = table.Column<DateTime>(nullable: false),
                    FacultyId = table.Column<int>(nullable: true),
                    CourseId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblFacultyCourses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tblFacultyCourses_tblProgrammeCourses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "tblProgrammeCourses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_tblFacultyCourses_tblFaculty_FacultyId",
                        column: x => x.FacultyId,
                        principalTable: "tblFaculty",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tblSchoolDepartments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastUpdated = table.Column<DateTime>(nullable: false),
                    SchoolId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblSchoolDepartments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tblSchoolDepartments_tblSchool_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "tblSchool",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tblSchoolProgrammeStudents",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastUpdated = table.Column<DateTime>(nullable: false),
                    ProgrammeId = table.Column<int>(nullable: true),
                    StudentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblSchoolProgrammeStudents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tblSchoolProgrammeStudents_tblSchoolProgrammes_ProgrammeId",
                        column: x => x.ProgrammeId,
                        principalTable: "tblSchoolProgrammes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_tblSchoolProgrammeStudents_tblStudent_StudentId",
                        column: x => x.StudentId,
                        principalTable: "tblStudent",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tblSchoolSettings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastUpdated = table.Column<DateTime>(nullable: false),
                    SchoolId = table.Column<int>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblSchoolSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tblSchoolSettings_tblSchool_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "tblSchool",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tblSchoolUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastUpdated = table.Column<DateTime>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    SchoolId = table.Column<int>(nullable: false),
                    CountryId = table.Column<int>(nullable: true),
                    StateId = table.Column<int>(nullable: true),
                    GenderId = table.Column<int>(nullable: true),
                    MemberRefereeId = table.Column<int>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsSchoolRegistered = table.Column<bool>(nullable: false),
                    DefaultPassword = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblSchoolUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tblSchoolUsers_tblCountryMaster_CountryId",
                        column: x => x.CountryId,
                        principalTable: "tblCountryMaster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_tblSchoolUsers_tblGenderMaster_GenderId",
                        column: x => x.GenderId,
                        principalTable: "tblGenderMaster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_tblSchoolUsers_tblSchool_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "tblSchool",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tblSchoolUsers_tblStateMaster_StateId",
                        column: x => x.StateId,
                        principalTable: "tblStateMaster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tblStudentSettings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastUpdated = table.Column<DateTime>(nullable: false),
                    StudentId = table.Column<int>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblStudentSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tblStudentSettings_tblStudent_StudentId",
                        column: x => x.StudentId,
                        principalTable: "tblStudent",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tblSysteMultimedia",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastUpdated = table.Column<DateTime>(nullable: false),
                    ContainsFile = table.Column<bool>(nullable: false),
                    ContentType = table.Column<string>(nullable: true),
                    FileData = table.Column<byte[]>(nullable: true),
                    FileUrl = table.Column<string>(nullable: true),
                    DataImg = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    SchoolId = table.Column<int>(nullable: true),
                    AdminId = table.Column<int>(nullable: true),
                    SyllabusId = table.Column<int>(nullable: true),
                    TopicId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblSysteMultimedia", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tblSysteMultimedia_tblAdmin_AdminId",
                        column: x => x.AdminId,
                        principalTable: "tblAdmin",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_tblSysteMultimedia_tblSchool_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "tblSchool",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_tblSysteMultimedia_tblCourseSyllabus_SyllabusId",
                        column: x => x.SyllabusId,
                        principalTable: "tblCourseSyllabus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_tblSysteMultimedia_tblSyllabusTopics_TopicId",
                        column: x => x.TopicId,
                        principalTable: "tblSyllabusTopics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tblFacultyDepartments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastUpdated = table.Column<DateTime>(nullable: false),
                    FacultyId = table.Column<int>(nullable: true),
                    DepartmentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblFacultyDepartments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tblFacultyDepartments_tblSchoolDepartments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "tblSchoolDepartments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_tblFacultyDepartments_tblFaculty_FacultyId",
                        column: x => x.FacultyId,
                        principalTable: "tblFaculty",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 16, 17, 39, 7, 516, DateTimeKind.Local), new DateTime(2019, 3, 16, 17, 39, 7, 516, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 16, 17, 39, 7, 516, DateTimeKind.Local), new DateTime(2019, 3, 16, 17, 39, 7, 516, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 16, 17, 39, 7, 516, DateTimeKind.Local), new DateTime(2019, 3, 16, 17, 39, 7, 516, DateTimeKind.Local) });

            migrationBuilder.CreateIndex(
                name: "IX_tblSchoolProgrammes_ProgramCoordinatorId",
                table: "tblSchoolProgrammes",
                column: "ProgramCoordinatorId");

            migrationBuilder.CreateIndex(
                name: "IX_tblFacultyCourses_CourseId",
                table: "tblFacultyCourses",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_tblFacultyCourses_FacultyId",
                table: "tblFacultyCourses",
                column: "FacultyId");

            migrationBuilder.CreateIndex(
                name: "IX_tblFacultyDepartments_DepartmentId",
                table: "tblFacultyDepartments",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_tblFacultyDepartments_FacultyId",
                table: "tblFacultyDepartments",
                column: "FacultyId");

            migrationBuilder.CreateIndex(
                name: "IX_tblSchoolDepartments_SchoolId",
                table: "tblSchoolDepartments",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_tblSchoolProgrammeStudents_ProgrammeId",
                table: "tblSchoolProgrammeStudents",
                column: "ProgrammeId");

            migrationBuilder.CreateIndex(
                name: "IX_tblSchoolProgrammeStudents_StudentId",
                table: "tblSchoolProgrammeStudents",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_tblSchoolSettings_SchoolId",
                table: "tblSchoolSettings",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_tblSchoolUsers_CountryId",
                table: "tblSchoolUsers",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_tblSchoolUsers_GenderId",
                table: "tblSchoolUsers",
                column: "GenderId");

            migrationBuilder.CreateIndex(
                name: "IX_tblSchoolUsers_SchoolId",
                table: "tblSchoolUsers",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_tblSchoolUsers_StateId",
                table: "tblSchoolUsers",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_tblStudentSettings_StudentId",
                table: "tblStudentSettings",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_tblSysteMultimedia_AdminId",
                table: "tblSysteMultimedia",
                column: "AdminId");

            migrationBuilder.CreateIndex(
                name: "IX_tblSysteMultimedia_SchoolId",
                table: "tblSysteMultimedia",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_tblSysteMultimedia_SyllabusId",
                table: "tblSysteMultimedia",
                column: "SyllabusId");

            migrationBuilder.CreateIndex(
                name: "IX_tblSysteMultimedia_TopicId",
                table: "tblSysteMultimedia",
                column: "TopicId");

            migrationBuilder.AddForeignKey(
                name: "FK_tblSchoolProgrammes_tblSchoolUsers_ProgramCoordinatorId",
                table: "tblSchoolProgrammes",
                column: "ProgramCoordinatorId",
                principalTable: "tblSchoolUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tblSchoolProgrammes_tblSchoolUsers_ProgramCoordinatorId",
                table: "tblSchoolProgrammes");

            migrationBuilder.DropTable(
                name: "tblFacultyCourses");

            migrationBuilder.DropTable(
                name: "tblFacultyDepartments");

            migrationBuilder.DropTable(
                name: "tblSchoolProgrammeStudents");

            migrationBuilder.DropTable(
                name: "tblSchoolSettings");

            migrationBuilder.DropTable(
                name: "tblSchoolUsers");

            migrationBuilder.DropTable(
                name: "tblStudentSettings");

            migrationBuilder.DropTable(
                name: "tblSysteMultimedia");

            migrationBuilder.DropTable(
                name: "tblSchoolDepartments");

            migrationBuilder.DropIndex(
                name: "IX_tblSchoolProgrammes_ProgramCoordinatorId",
                table: "tblSchoolProgrammes");

            migrationBuilder.DropColumn(
                name: "Duration",
                table: "tblSyllabusTopics");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "tblSchoolProgrammes");

            migrationBuilder.DropColumn(
                name: "Duration",
                table: "tblSchoolProgrammes");

            migrationBuilder.DropColumn(
                name: "ProgramCoordinatorId",
                table: "tblSchoolProgrammes");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "tblProgrammeCourses");

            migrationBuilder.DropColumn(
                name: "Credit",
                table: "tblProgrammeCourses");

            migrationBuilder.DropColumn(
                name: "Duration",
                table: "tblProgrammeCourses");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 10, 22, 53, 11, 835, DateTimeKind.Local), new DateTime(2019, 3, 10, 22, 53, 11, 835, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 10, 22, 53, 11, 835, DateTimeKind.Local), new DateTime(2019, 3, 10, 22, 53, 11, 835, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 10, 22, 53, 11, 835, DateTimeKind.Local), new DateTime(2019, 3, 10, 22, 53, 11, 835, DateTimeKind.Local) });
        }
    }
}
