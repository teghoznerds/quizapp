﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class SyllabusStudents : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tblCourseSyllabusStudents",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastUpdated = table.Column<DateTime>(nullable: false),
                    SyllabusId = table.Column<int>(nullable: true),
                    StudentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblCourseSyllabusStudents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tblCourseSyllabusStudents_tblStudent_StudentId",
                        column: x => x.StudentId,
                        principalTable: "tblStudent",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_tblCourseSyllabusStudents_tblCourseSyllabus_SyllabusId",
                        column: x => x.SyllabusId,
                        principalTable: "tblCourseSyllabus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 21, 15, 3, 30, 349, DateTimeKind.Local), new DateTime(2019, 3, 21, 15, 3, 30, 349, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 21, 15, 3, 30, 349, DateTimeKind.Local), new DateTime(2019, 3, 21, 15, 3, 30, 349, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 21, 15, 3, 30, 349, DateTimeKind.Local), new DateTime(2019, 3, 21, 15, 3, 30, 349, DateTimeKind.Local) });

            migrationBuilder.CreateIndex(
                name: "IX_tblCourseSyllabusStudents_StudentId",
                table: "tblCourseSyllabusStudents",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_tblCourseSyllabusStudents_SyllabusId",
                table: "tblCourseSyllabusStudents",
                column: "SyllabusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblCourseSyllabusStudents");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 15, 1, 55, 326, DateTimeKind.Local), new DateTime(2019, 3, 20, 15, 1, 55, 326, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 15, 1, 55, 326, DateTimeKind.Local), new DateTime(2019, 3, 20, 15, 1, 55, 326, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 15, 1, 55, 326, DateTimeKind.Local), new DateTime(2019, 3, 20, 15, 1, 55, 326, DateTimeKind.Local) });
        }
    }
}
