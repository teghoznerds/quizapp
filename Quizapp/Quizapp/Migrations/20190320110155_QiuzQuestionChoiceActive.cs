﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class QiuzQuestionChoiceActive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "tblQuizzesQuestionChoices",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsCorrect",
                table: "tblQuizzesQuestionChoices",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 15, 1, 55, 326, DateTimeKind.Local), new DateTime(2019, 3, 20, 15, 1, 55, 326, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 15, 1, 55, 326, DateTimeKind.Local), new DateTime(2019, 3, 20, 15, 1, 55, 326, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 15, 1, 55, 326, DateTimeKind.Local), new DateTime(2019, 3, 20, 15, 1, 55, 326, DateTimeKind.Local) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "tblQuizzesQuestionChoices");

            migrationBuilder.DropColumn(
                name: "IsCorrect",
                table: "tblQuizzesQuestionChoices");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 14, 13, 15, 560, DateTimeKind.Local), new DateTime(2019, 3, 20, 14, 13, 15, 560, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 14, 13, 15, 560, DateTimeKind.Local), new DateTime(2019, 3, 20, 14, 13, 15, 560, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 14, 13, 15, 560, DateTimeKind.Local), new DateTime(2019, 3, 20, 14, 13, 15, 560, DateTimeKind.Local) });
        }
    }
}
