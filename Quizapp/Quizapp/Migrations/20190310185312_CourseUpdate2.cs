﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class CourseUpdate2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tblTopicQuizzes_tblSyllabusTopics_SyllabusId",
                table: "tblTopicQuizzes");

            migrationBuilder.AlterColumn<int>(
                name: "SyllabusId",
                table: "tblTopicQuizzes",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "tblTopicQuizzes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "SyllabusTopicId",
                table: "tblTopicQuizzes",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 10, 22, 53, 11, 835, DateTimeKind.Local), new DateTime(2019, 3, 10, 22, 53, 11, 835, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 10, 22, 53, 11, 835, DateTimeKind.Local), new DateTime(2019, 3, 10, 22, 53, 11, 835, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 10, 22, 53, 11, 835, DateTimeKind.Local), new DateTime(2019, 3, 10, 22, 53, 11, 835, DateTimeKind.Local) });

            migrationBuilder.AddForeignKey(
                name: "FK_tblTopicQuizzes_tblSyllabusTopics_SyllabusId",
                table: "tblTopicQuizzes",
                column: "SyllabusId",
                principalTable: "tblSyllabusTopics",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tblTopicQuizzes_tblSyllabusTopics_SyllabusId",
                table: "tblTopicQuizzes");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "tblTopicQuizzes");

            migrationBuilder.DropColumn(
                name: "SyllabusTopicId",
                table: "tblTopicQuizzes");

            migrationBuilder.AlterColumn<int>(
                name: "SyllabusId",
                table: "tblTopicQuizzes",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 10, 22, 16, 45, 546, DateTimeKind.Local), new DateTime(2019, 3, 10, 22, 16, 45, 546, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 10, 22, 16, 45, 546, DateTimeKind.Local), new DateTime(2019, 3, 10, 22, 16, 45, 546, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 10, 22, 16, 45, 546, DateTimeKind.Local), new DateTime(2019, 3, 10, 22, 16, 45, 546, DateTimeKind.Local) });

            migrationBuilder.AddForeignKey(
                name: "FK_tblTopicQuizzes_tblSyllabusTopics_SyllabusId",
                table: "tblTopicQuizzes",
                column: "SyllabusId",
                principalTable: "tblSyllabusTopics",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
