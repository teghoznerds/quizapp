﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class PorgramIsActive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "tblSchoolProgrammes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 11, 48, 135, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 11, 48, 135, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 11, 48, 135, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 11, 48, 135, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 11, 48, 135, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 11, 48, 135, DateTimeKind.Local) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "tblSchoolProgrammes");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 16, 17, 39, 7, 516, DateTimeKind.Local), new DateTime(2019, 3, 16, 17, 39, 7, 516, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 16, 17, 39, 7, 516, DateTimeKind.Local), new DateTime(2019, 3, 16, 17, 39, 7, 516, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 16, 17, 39, 7, 516, DateTimeKind.Local), new DateTime(2019, 3, 16, 17, 39, 7, 516, DateTimeKind.Local) });
        }
    }
}
