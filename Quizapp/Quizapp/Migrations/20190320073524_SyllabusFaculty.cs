﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class SyllabusFaculty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FacultyId",
                table: "tblCourseSyllabus",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 11, 35, 24, 503, DateTimeKind.Local), new DateTime(2019, 3, 20, 11, 35, 24, 503, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 11, 35, 24, 503, DateTimeKind.Local), new DateTime(2019, 3, 20, 11, 35, 24, 503, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 11, 35, 24, 503, DateTimeKind.Local), new DateTime(2019, 3, 20, 11, 35, 24, 503, DateTimeKind.Local) });

            migrationBuilder.CreateIndex(
                name: "IX_tblCourseSyllabus_FacultyId",
                table: "tblCourseSyllabus",
                column: "FacultyId");

            migrationBuilder.AddForeignKey(
                name: "FK_tblCourseSyllabus_tblFaculty_FacultyId",
                table: "tblCourseSyllabus",
                column: "FacultyId",
                principalTable: "tblFaculty",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tblCourseSyllabus_tblFaculty_FacultyId",
                table: "tblCourseSyllabus");

            migrationBuilder.DropIndex(
                name: "IX_tblCourseSyllabus_FacultyId",
                table: "tblCourseSyllabus");

            migrationBuilder.DropColumn(
                name: "FacultyId",
                table: "tblCourseSyllabus");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 11, 32, 33, 469, DateTimeKind.Local), new DateTime(2019, 3, 20, 11, 32, 33, 469, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 11, 32, 33, 469, DateTimeKind.Local), new DateTime(2019, 3, 20, 11, 32, 33, 469, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 11, 32, 33, 469, DateTimeKind.Local), new DateTime(2019, 3, 20, 11, 32, 33, 469, DateTimeKind.Local) });
        }
    }
}
