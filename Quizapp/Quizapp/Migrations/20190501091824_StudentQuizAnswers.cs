﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class StudentQuizAnswers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tblStudentQuiz",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastUpdated = table.Column<DateTime>(nullable: false),
                    StudentId = table.Column<int>(nullable: false),
                    QuizId = table.Column<int>(nullable: false),
                    Result = table.Column<double>(nullable: false),
                    IsSubmitted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblStudentQuiz", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tblStudentQuiz_tblTopicQuizzes_QuizId",
                        column: x => x.QuizId,
                        principalTable: "tblTopicQuizzes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tblStudentQuiz_tblStudent_StudentId",
                        column: x => x.StudentId,
                        principalTable: "tblStudent",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tblStudentQuizAnswers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastUpdated = table.Column<DateTime>(nullable: false),
                    StudentQuizId = table.Column<int>(nullable: false),
                    QuestionId = table.Column<int>(nullable: false),
                    Answer = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblStudentQuizAnswers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tblStudentQuizAnswers_tblQuizzesQuestions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "tblQuizzesQuestions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tblStudentQuizAnswers_tblStudentQuiz_StudentQuizId",
                        column: x => x.StudentQuizId,
                        principalTable: "tblStudentQuiz",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 5, 1, 13, 18, 23, 791, DateTimeKind.Local), new DateTime(2019, 5, 1, 13, 18, 23, 791, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 5, 1, 13, 18, 23, 791, DateTimeKind.Local), new DateTime(2019, 5, 1, 13, 18, 23, 791, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 5, 1, 13, 18, 23, 791, DateTimeKind.Local), new DateTime(2019, 5, 1, 13, 18, 23, 791, DateTimeKind.Local) });

            migrationBuilder.CreateIndex(
                name: "IX_tblStudentQuiz_QuizId",
                table: "tblStudentQuiz",
                column: "QuizId");

            migrationBuilder.CreateIndex(
                name: "IX_tblStudentQuiz_StudentId",
                table: "tblStudentQuiz",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_tblStudentQuizAnswers_QuestionId",
                table: "tblStudentQuizAnswers",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_tblStudentQuizAnswers_StudentQuizId",
                table: "tblStudentQuizAnswers",
                column: "StudentQuizId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblStudentQuizAnswers");

            migrationBuilder.DropTable(
                name: "tblStudentQuiz");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 22, 13, 29, 8, 651, DateTimeKind.Local), new DateTime(2019, 3, 22, 13, 29, 8, 651, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 22, 13, 29, 8, 651, DateTimeKind.Local), new DateTime(2019, 3, 22, 13, 29, 8, 651, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 22, 13, 29, 8, 651, DateTimeKind.Local), new DateTime(2019, 3, 22, 13, 29, 8, 651, DateTimeKind.Local) });
        }
    }
}
