﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class QiuzFKupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tblTopicQuizzes_tblSyllabusTopics_SyllabusId",
                table: "tblTopicQuizzes");

            migrationBuilder.DropIndex(
                name: "IX_tblTopicQuizzes_SyllabusId",
                table: "tblTopicQuizzes");

            migrationBuilder.DropColumn(
                name: "SyllabusId",
                table: "tblTopicQuizzes");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 12, 8, 31, 948, DateTimeKind.Local), new DateTime(2019, 3, 20, 12, 8, 31, 948, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 12, 8, 31, 948, DateTimeKind.Local), new DateTime(2019, 3, 20, 12, 8, 31, 948, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 12, 8, 31, 948, DateTimeKind.Local), new DateTime(2019, 3, 20, 12, 8, 31, 948, DateTimeKind.Local) });

            migrationBuilder.CreateIndex(
                name: "IX_tblTopicQuizzes_SyllabusTopicId",
                table: "tblTopicQuizzes",
                column: "SyllabusTopicId");

            migrationBuilder.AddForeignKey(
                name: "FK_tblTopicQuizzes_tblSyllabusTopics_SyllabusTopicId",
                table: "tblTopicQuizzes",
                column: "SyllabusTopicId",
                principalTable: "tblSyllabusTopics",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tblTopicQuizzes_tblSyllabusTopics_SyllabusTopicId",
                table: "tblTopicQuizzes");

            migrationBuilder.DropIndex(
                name: "IX_tblTopicQuizzes_SyllabusTopicId",
                table: "tblTopicQuizzes");

            migrationBuilder.AddColumn<int>(
                name: "SyllabusId",
                table: "tblTopicQuizzes",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 11, 50, 35, 704, DateTimeKind.Local), new DateTime(2019, 3, 20, 11, 50, 35, 704, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 11, 50, 35, 704, DateTimeKind.Local), new DateTime(2019, 3, 20, 11, 50, 35, 704, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 11, 50, 35, 704, DateTimeKind.Local), new DateTime(2019, 3, 20, 11, 50, 35, 704, DateTimeKind.Local) });

            migrationBuilder.CreateIndex(
                name: "IX_tblTopicQuizzes_SyllabusId",
                table: "tblTopicQuizzes",
                column: "SyllabusId");

            migrationBuilder.AddForeignKey(
                name: "FK_tblTopicQuizzes_tblSyllabusTopics_SyllabusId",
                table: "tblTopicQuizzes",
                column: "SyllabusId",
                principalTable: "tblSyllabusTopics",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
