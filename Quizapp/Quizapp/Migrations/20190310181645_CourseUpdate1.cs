﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class CourseUpdate1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "tblCourseSyllabus",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 10, 22, 16, 45, 546, DateTimeKind.Local), new DateTime(2019, 3, 10, 22, 16, 45, 546, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 10, 22, 16, 45, 546, DateTimeKind.Local), new DateTime(2019, 3, 10, 22, 16, 45, 546, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 10, 22, 16, 45, 546, DateTimeKind.Local), new DateTime(2019, 3, 10, 22, 16, 45, 546, DateTimeKind.Local) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "tblCourseSyllabus");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 10, 22, 5, 34, 400, DateTimeKind.Local), new DateTime(2019, 3, 10, 22, 5, 34, 400, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 10, 22, 5, 34, 400, DateTimeKind.Local), new DateTime(2019, 3, 10, 22, 5, 34, 400, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 10, 22, 5, 34, 400, DateTimeKind.Local), new DateTime(2019, 3, 10, 22, 5, 34, 400, DateTimeKind.Local) });
        }
    }
}
