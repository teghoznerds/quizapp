﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class RemovedAdminregistered : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSchoolRegistered",
                table: "tblSchoolUsers");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 21, 41, 744, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 21, 41, 744, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 21, 41, 744, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 21, 41, 744, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 21, 41, 744, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 21, 41, 744, DateTimeKind.Local) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSchoolRegistered",
                table: "tblSchoolUsers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 11, 48, 135, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 11, 48, 135, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 11, 48, 135, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 11, 48, 135, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 11, 48, 135, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 11, 48, 135, DateTimeKind.Local) });
        }
    }
}
