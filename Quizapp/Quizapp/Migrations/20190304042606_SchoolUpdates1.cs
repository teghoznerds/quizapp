﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class SchoolUpdates1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "tblSchool",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Username",
                table: "tblSchool",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Zip",
                table: "tblSchool",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 4, 8, 26, 6, 33, DateTimeKind.Local), new DateTime(2019, 3, 4, 8, 26, 6, 33, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 4, 8, 26, 6, 33, DateTimeKind.Local), new DateTime(2019, 3, 4, 8, 26, 6, 33, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 4, 8, 26, 6, 33, DateTimeKind.Local), new DateTime(2019, 3, 4, 8, 26, 6, 33, DateTimeKind.Local) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Password",
                table: "tblSchool");

            migrationBuilder.DropColumn(
                name: "Username",
                table: "tblSchool");

            migrationBuilder.DropColumn(
                name: "Zip",
                table: "tblSchool");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 2, 15, 8, 51, 331, DateTimeKind.Local), new DateTime(2019, 3, 2, 15, 8, 51, 331, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 2, 15, 8, 51, 331, DateTimeKind.Local), new DateTime(2019, 3, 2, 15, 8, 51, 331, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 2, 15, 8, 51, 331, DateTimeKind.Local), new DateTime(2019, 3, 2, 15, 8, 51, 331, DateTimeKind.Local) });
        }
    }
}
