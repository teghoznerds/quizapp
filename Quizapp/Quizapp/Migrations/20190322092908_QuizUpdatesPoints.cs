﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class QuizUpdatesPoints : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "ExpectedDuration",
                table: "tblTopicQuizzes",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TotalMarkPoint",
                table: "tblTopicQuizzes",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "MarkPoint",
                table: "tblQuizzesQuestions",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 22, 13, 29, 8, 651, DateTimeKind.Local), new DateTime(2019, 3, 22, 13, 29, 8, 651, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 22, 13, 29, 8, 651, DateTimeKind.Local), new DateTime(2019, 3, 22, 13, 29, 8, 651, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 22, 13, 29, 8, 651, DateTimeKind.Local), new DateTime(2019, 3, 22, 13, 29, 8, 651, DateTimeKind.Local) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExpectedDuration",
                table: "tblTopicQuizzes");

            migrationBuilder.DropColumn(
                name: "TotalMarkPoint",
                table: "tblTopicQuizzes");

            migrationBuilder.DropColumn(
                name: "MarkPoint",
                table: "tblQuizzesQuestions");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 21, 15, 3, 30, 349, DateTimeKind.Local), new DateTime(2019, 3, 21, 15, 3, 30, 349, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 21, 15, 3, 30, 349, DateTimeKind.Local), new DateTime(2019, 3, 21, 15, 3, 30, 349, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 21, 15, 3, 30, 349, DateTimeKind.Local), new DateTime(2019, 3, 21, 15, 3, 30, 349, DateTimeKind.Local) });
        }
    }
}
