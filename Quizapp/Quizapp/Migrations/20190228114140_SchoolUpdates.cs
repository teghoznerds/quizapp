﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class SchoolUpdates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "tblSchool",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "tblSchool",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "tblSchool",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "tblSchool",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "tblSchool",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "tblSchool",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Website",
                table: "tblSchool",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 2, 28, 15, 41, 40, 377, DateTimeKind.Local), new DateTime(2019, 2, 28, 15, 41, 40, 377, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 2, 28, 15, 41, 40, 377, DateTimeKind.Local), new DateTime(2019, 2, 28, 15, 41, 40, 377, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 2, 28, 15, 41, 40, 377, DateTimeKind.Local), new DateTime(2019, 2, 28, 15, 41, 40, 377, DateTimeKind.Local) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "tblSchool");

            migrationBuilder.DropColumn(
                name: "City",
                table: "tblSchool");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "tblSchool");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "tblSchool");

            migrationBuilder.DropColumn(
                name: "State",
                table: "tblSchool");

            migrationBuilder.DropColumn(
                name: "Url",
                table: "tblSchool");

            migrationBuilder.DropColumn(
                name: "Website",
                table: "tblSchool");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 2, 15, 14, 50, 55, 299, DateTimeKind.Local), new DateTime(2019, 2, 15, 14, 50, 55, 299, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 2, 15, 14, 50, 55, 299, DateTimeKind.Local), new DateTime(2019, 2, 15, 14, 50, 55, 299, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 2, 15, 14, 50, 55, 299, DateTimeKind.Local), new DateTime(2019, 2, 15, 14, 50, 55, 299, DateTimeKind.Local) });
        }
    }
}
