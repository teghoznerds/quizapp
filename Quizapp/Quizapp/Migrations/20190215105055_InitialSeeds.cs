﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class InitialSeeds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AnswerType",
                table: "tblQuizzesQuestions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.InsertData(
                table: "tblGenderMaster",
                columns: new[] { "Id", "DateCreated", "DateLastUpdated", "Gender", "IsActive" },
                values: new object[] { 1, new DateTime(2019, 2, 15, 14, 50, 55, 299, DateTimeKind.Local), new DateTime(2019, 2, 15, 14, 50, 55, 299, DateTimeKind.Local), "Male", false });

            migrationBuilder.InsertData(
                table: "tblGenderMaster",
                columns: new[] { "Id", "DateCreated", "DateLastUpdated", "Gender", "IsActive" },
                values: new object[] { 2, new DateTime(2019, 2, 15, 14, 50, 55, 299, DateTimeKind.Local), new DateTime(2019, 2, 15, 14, 50, 55, 299, DateTimeKind.Local), "Female", false });

            migrationBuilder.InsertData(
                table: "tblSchool",
                columns: new[] { "Id", "DateCreated", "DateLastUpdated", "Name" },
                values: new object[] { 1, new DateTime(2019, 2, 15, 14, 50, 55, 299, DateTimeKind.Local), new DateTime(2019, 2, 15, 14, 50, 55, 299, DateTimeKind.Local), "Middlesex University" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DropColumn(
                name: "AnswerType",
                table: "tblQuizzesQuestions");
        }
    }
}
