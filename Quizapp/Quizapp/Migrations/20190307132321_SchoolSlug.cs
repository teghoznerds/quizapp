﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class SchoolSlug : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Slug",
                table: "tblSchool",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 7, 17, 23, 20, 586, DateTimeKind.Local), new DateTime(2019, 3, 7, 17, 23, 20, 586, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 7, 17, 23, 20, 586, DateTimeKind.Local), new DateTime(2019, 3, 7, 17, 23, 20, 586, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 7, 17, 23, 20, 586, DateTimeKind.Local), new DateTime(2019, 3, 7, 17, 23, 20, 586, DateTimeKind.Local) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Slug",
                table: "tblSchool");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 7, 15, 55, 6, 624, DateTimeKind.Local), new DateTime(2019, 3, 7, 15, 55, 6, 624, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 7, 15, 55, 6, 624, DateTimeKind.Local), new DateTime(2019, 3, 7, 15, 55, 6, 624, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 7, 15, 55, 6, 624, DateTimeKind.Local), new DateTime(2019, 3, 7, 15, 55, 6, 624, DateTimeKind.Local) });
        }
    }
}
