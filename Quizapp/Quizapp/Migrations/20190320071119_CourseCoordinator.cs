﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class CourseCoordinator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CourseCoordinatorId",
                table: "tblProgrammeCourses",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 11, 11, 18, 606, DateTimeKind.Local), new DateTime(2019, 3, 20, 11, 11, 18, 606, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 11, 11, 18, 606, DateTimeKind.Local), new DateTime(2019, 3, 20, 11, 11, 18, 606, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 20, 11, 11, 18, 606, DateTimeKind.Local), new DateTime(2019, 3, 20, 11, 11, 18, 606, DateTimeKind.Local) });

            migrationBuilder.CreateIndex(
                name: "IX_tblProgrammeCourses_CourseCoordinatorId",
                table: "tblProgrammeCourses",
                column: "CourseCoordinatorId");

            migrationBuilder.AddForeignKey(
                name: "FK_tblProgrammeCourses_tblSchoolUsers_CourseCoordinatorId",
                table: "tblProgrammeCourses",
                column: "CourseCoordinatorId",
                principalTable: "tblSchoolUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tblProgrammeCourses_tblSchoolUsers_CourseCoordinatorId",
                table: "tblProgrammeCourses");

            migrationBuilder.DropIndex(
                name: "IX_tblProgrammeCourses_CourseCoordinatorId",
                table: "tblProgrammeCourses");

            migrationBuilder.DropColumn(
                name: "CourseCoordinatorId",
                table: "tblProgrammeCourses");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 26, 41, 99, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 26, 41, 99, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 26, 41, 99, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 26, 41, 99, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 26, 41, 99, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 26, 41, 99, DateTimeKind.Local) });
        }
    }
}
