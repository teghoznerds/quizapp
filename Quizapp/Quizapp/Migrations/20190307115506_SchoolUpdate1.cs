﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class SchoolUpdate1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountNumber",
                table: "tblStudent");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "tblStudent");

            migrationBuilder.DropColumn(
                name: "RelationshipStatusId",
                table: "tblStudent");

            migrationBuilder.RenameColumn(
                name: "IsAdminRegistered",
                table: "tblStudent",
                newName: "IsSchoolRegistered");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 7, 15, 55, 6, 624, DateTimeKind.Local), new DateTime(2019, 3, 7, 15, 55, 6, 624, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 7, 15, 55, 6, 624, DateTimeKind.Local), new DateTime(2019, 3, 7, 15, 55, 6, 624, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 7, 15, 55, 6, 624, DateTimeKind.Local), new DateTime(2019, 3, 7, 15, 55, 6, 624, DateTimeKind.Local) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsSchoolRegistered",
                table: "tblStudent",
                newName: "IsAdminRegistered");

            migrationBuilder.AddColumn<string>(
                name: "AccountNumber",
                table: "tblStudent",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "tblStudent",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RelationshipStatusId",
                table: "tblStudent",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 7, 15, 17, 21, 986, DateTimeKind.Local), new DateTime(2019, 3, 7, 15, 17, 21, 986, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 7, 15, 17, 21, 986, DateTimeKind.Local), new DateTime(2019, 3, 7, 15, 17, 21, 986, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 7, 15, 17, 21, 986, DateTimeKind.Local), new DateTime(2019, 3, 7, 15, 17, 21, 986, DateTimeKind.Local) });
        }
    }
}
