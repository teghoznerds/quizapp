﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Quizapp.Migrations
{
    public partial class ApplicationUsert : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SchoolUserId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 26, 41, 99, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 26, 41, 99, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 26, 41, 99, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 26, 41, 99, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 26, 41, 99, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 26, 41, 99, DateTimeKind.Local) });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_SchoolUserId",
                table: "AspNetUsers",
                column: "SchoolUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_tblSchoolUsers_SchoolUserId",
                table: "AspNetUsers",
                column: "SchoolUserId",
                principalTable: "tblSchoolUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_tblSchoolUsers_SchoolUserId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_SchoolUserId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "SchoolUserId",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 21, 41, 744, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 21, 41, 744, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblGenderMaster",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 21, 41, 744, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 21, 41, 744, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "tblSchool",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DateCreated", "DateLastUpdated" },
                values: new object[] { new DateTime(2019, 3, 19, 17, 21, 41, 744, DateTimeKind.Local), new DateTime(2019, 3, 19, 17, 21, 41, 744, DateTimeKind.Local) });
        }
    }
}
