﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quizapp.Models;

namespace Quizapp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().QuizAppSeedAsync().Result.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>

        WebHost.CreateDefaultBuilder(args)
            .UseKestrel(options =>
            {
                //options.Limits.MaxRequestBodySize = long.MaxValue;
                options.Limits.MaxRequestBodySize = null;
            })
                //.UseUrls("http://127.0.0.1:5010")
                .UseStartup<Startup>()
            .UseContentRoot(Directory.GetCurrentDirectory())
            .UseConfiguration(new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("hosting.json", optional: true).Build())
            .UseIISIntegration();
    }
}
