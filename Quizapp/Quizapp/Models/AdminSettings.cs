﻿using Quizapp.Models;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Quizapp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quizapp.Models
{

    public class AdminSettings
    {
        public string DefaultCulture { get { return getSetting(nameof(DefaultCulture), "en-GB"); } set { setSetting(nameof(DefaultCulture), value); } }
        public string ProjectName { get { return getSetting(nameof(ProjectName), "QUIZAPP"); } set { setSetting(nameof(ProjectName), value); } }
        public string CompanyName { get { return getSetting(nameof(ProjectName), "QUIZAPP"); } set { setSetting(nameof(CompanyName), value); } }
        public string CompanyAddress { get { return getSetting(nameof(CompanyAddress), "Plot 859 Bishop Aboyade cole st, V.I, Lagos Lagos, 23401 Nigeria"); } set { setSetting(nameof(CompanyAddress), value); } }
        public string CompanyPhone { get { return getSetting(nameof(CompanyPhone), "+(234) 802 685 5691"); } set { setSetting(nameof(CompanyPhone), value); } }
        public string SigningDomain { get { return getSetting(nameof(SigningDomain), "cashboxng.com"); } set { setSetting(nameof(SigningDomain), value); } }
        public string ProjectDescription { get { return getSetting(nameof(ProjectDescription), "CASHBOX"); } set { setSetting(nameof(ProjectDescription), value); } }
        public string ProjectThemeColor { get { return getSetting(nameof(ProjectThemeColor), "#072e68"); } set { setSetting(nameof(ProjectThemeColor), value); } }
        public string GoogleSiteVerification{ get { return getSetting(nameof(GoogleSiteVerification), "MExL7WtcYncP1wqoMLitYen57hOhaWzUhJ2r2flueKs"); } set { setSetting(nameof(GoogleSiteVerification), value); } }       
        public string MemberCodePrefix { get { return getSetting(nameof(MemberCodePrefix), "CM"); } set { setSetting(nameof(MemberCodePrefix), value); } }
        public string MemberSavingPlanPrefix { get { return getSetting(nameof(MemberSavingPlanPrefix), "CMP"); } set { setSetting(nameof(MemberSavingPlanPrefix), value); } }
        public string AdminCodePrefix { get { return getSetting(nameof(AdminCodePrefix), "AM"); } set { setSetting(nameof(AdminCodePrefix), value); } }
        public string TransactionCodePrefix { get { return getSetting(nameof(TransactionCodePrefix), "CX"); } set { setSetting(nameof(TransactionCodePrefix), value); } }
        public int DefaultSessionTime { get { return getSetting(nameof(DefaultSessionTime), 10); } set { setSetting(nameof(DefaultSessionTime), value); } }
        public string DefaultCurrencyDecimalPoints { get { return getSetting(nameof(DefaultCurrencyDecimalPoints), "N2"); } set { setSetting(nameof(DefaultCurrencyDecimalPoints), value); } }
        public string DefaultCurrency { get { return getSetting(nameof(DefaultCurrency), "NGN"); } set { setSetting(nameof(DefaultCurrency), value); } }
        public string DefaultCountryFullName { get { return getSetting(nameof(DefaultCountryFullName), "Nigeria"); } set { setSetting(nameof(DefaultCountryFullName), value); } }
        public string DefaultCountry { get { return getSetting(nameof(DefaultCountry), "NG"); } set { setSetting(nameof(DefaultCountry), value); } }
        public string DefaultNiaraSubscriptionAmount { get { return getSetting(nameof(DefaultNiaraSubscriptionAmount), "100"); } set { setSetting(nameof(DefaultNiaraSubscriptionAmount), value); } }
        public string bulksmsnigeriaUsername { get { return getSetting(nameof(bulksmsnigeriaUsername), "aig.sydney@yahoo.com"); } set { setSetting(nameof(bulksmsnigeriaUsername), value); } }
        public string bulksmsnigeriaPassword { get { return getSetting(nameof(bulksmsnigeriaPassword), "Macwindows1"); } set { setSetting(nameof(bulksmsnigeriaPassword), value); } }
        public string bulksmsnigeriaUrl { get { return getSetting(nameof(bulksmsnigeriaUrl), "https://portal.bulksmsnigeria.net/api/"); } set { setSetting(nameof(bulksmsnigeriaUrl), value); } }
        public string smsMessageTextTA { get { return getSetting(nameof(smsMessageTextTA), "Your confirmation token is: "); } set { setSetting(nameof(smsMessageTextTA), value); } }
        public int PasswordMinimumPasswordLength { get { return getSetting(nameof(PasswordMinimumPasswordLength), 6); } set { setSetting(nameof(PasswordMinimumPasswordLength), value); } }
        public bool PasswordRequireUpperCase { get { return getSetting(nameof(PasswordRequireUpperCase), true); } set { setSetting(nameof(PasswordRequireUpperCase), value); } }
        public bool PasswordRequireDigit { get { return getSetting(nameof(PasswordRequireDigit), true); } set { setSetting(nameof(PasswordRequireDigit), value); } }
        public bool PasswordRequireLowerCase { get { return getSetting(nameof(PasswordRequireLowerCase), true); } set { setSetting(nameof(PasswordRequireLowerCase), value); } }
        public bool PasswordRequireNonAlphaNumeric { get { return getSetting(nameof(PasswordRequireNonAlphaNumeric), true); } set { setSetting(nameof(PasswordRequireNonAlphaNumeric), value); } }
        public int PasswordRequireUniqueCharacters { get { return getSetting(nameof(PasswordRequireUniqueCharacters), 1); } set { setSetting(nameof(PasswordRequireUniqueCharacters), value); } }
        public int PasswordMaximumFailedAttempts { get { return getSetting(nameof(PasswordMaximumFailedAttempts), 5); } set { setSetting(nameof(PasswordMaximumFailedAttempts), value); } }
        public int DefaultLockedTimespan { get { return getSetting(nameof(DefaultLockedTimespan), 5); } set { setSetting(nameof(DefaultLockedTimespan), value); } }
        //SHOULD BE OPTIMIZED LATER
        public AdminSettings()
        {
            reload();
        }

        #region getsetcache
        private string getSetting(string key)
        {
            return get(key, "");
        }

        private string getSetting(string key, string default_value)
        {
            return get(key, default_value);
        }

        private int getSetting(string key, int default_value)
        {
            try
            {
                return int.Parse(get(key, default_value.ToString()));
            }
            catch
            {
                return default_value;
            }
        }

        private decimal getSetting(string key, decimal default_value)
        {
            try
            {
                return decimal.Parse(get(key, default_value.ToString("F04")));
            }
            catch
            {
                return default_value;
            }
        }

        private bool getSetting(string key, bool default_value)
        {
            try
            {
                return bool.Parse(get(key, default_value.ToString()));
            }
            catch
            {
                return default_value;
            }
        }

        private void setSetting(string key, string value)
        {
            save(key, value);
        }

        private void setSetting(string key, bool value)
        {
            save(key, value.ToString());
        }

        private void setSetting(string key, decimal value)
        {
            save(key, value.ToString("F04"));
        }

        private void setSetting(string key, int value)
        {
            save(key, value.ToString());
        }

        private string get(string key, string default_value)
        {
            try
            {
                var setting = GetAllCachedAdminSettings().FirstOrDefault(f => f.Key == key);
                save(key, setting != null ? setting.Value : default_value);
                return setting != null ? setting.Value : default_value;
            }
            catch (Exception)
            {
                return default_value;
            }
        }

        private const string adminSettingsCacheKey = "quizapp-settings";
        MemoryCacheEntryOptions cacheOptions = new MemoryCacheEntryOptions
        {
            SlidingExpiration = TimeSpan.FromMinutes(30),
            Priority = CacheItemPriority.NeverRemove
        };
        MemoryCache memorycache = new MemoryCache(new MemoryCacheOptions());

        private IEnumerable<QuizappSetting> GetAllCachedAdminSettings()
        {
            var db = ContextManager.GetQuizappContext();
            if ((db.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).Exists())
            {
                if (Utilities.CheckDatabaseMigrationCompleted(db))
                {
                    return memorycache.FetchAndCache<IEnumerable<QuizappSetting>>(adminSettingsCacheKey, () => db.tblQuizappSetting.ToList(), cacheOptions);
                }
            }
            return memorycache.FetchAndCache<IEnumerable<QuizappSetting>>(adminSettingsCacheKey, () => new List<QuizappSetting>(), cacheOptions);
        }

        private QuizappContext db;

        private void save(string key, string value)
        {
            db = ContextManager.GetQuizappContext();
            if (db.tblQuizappSetting.Count(c => c.Key == key) == 1)
            {
                db.tblQuizappSetting.Single(c => c.Key == key).Value = (value ?? "");
                db.SaveChanges();
            }
            else
            {
                var c = new QuizappSetting { Key = key, Value = (value ?? "") };
                db.tblQuizappSetting.Add(c);
                db.SaveChanges();
            }

            memorycache.Remove(adminSettingsCacheKey);
        }

        private void reload()
        {
            memorycache.Remove(adminSettingsCacheKey);
            GetAllCachedAdminSettings();
        }
        #endregion        
    }

    public static class QuizappSettings
    {
        public static AdminSettings Setting()
        {
            return new AdminSettings();
        }
    }
}
