﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using WebEssentials.AspNetCore.OutputCaching;
using WebEssentials.AspNetCore.Pwa;
using WebMarkupMin.AspNetCore2;
using WebMarkupMin.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using IWmmLogger = WebMarkupMin.Core.Loggers.ILogger;
using WmmNullLogger = WebMarkupMin.Core.Loggers.NullLogger;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;

namespace Quizapp.Models
{
    public static class ServiceConfigurations
    {
        public static void Set(IServiceCollection services)
        {
            DatabaseIdentityConfig(services);
            //ProgressiveWebOptions(services);
            WebOptimizationOptions(services);
            //CookieOptions(services);
            Mailers(services);
            Authorizations(services);
            SystemDetections(services);
            RealTime(services);
            CustomSettings(services);
            Logging(services);
        }
        public static void DatabaseIdentityConfig(IServiceCollection services)
        {
            //services.AddScoped<SignInManager<ApplicationUser>, SignInManager<ApplicationUser>>();
            //services.AddScoped<RoleManager<IdentityRole>>();

            services.AddDbContext<QuizappContext>(options =>
                options.UseSqlServer(
                    ConnectionManager.Connection["ConnectionString"]).EnableSensitiveDataLogging())
                    .AddIdentity<ApplicationUser, IdentityRole>().AddEntityFrameworkStores<QuizappContext>()
                    .AddDefaultTokenProviders()
                    .AddEntityFrameworkStores<QuizappContext>();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings.
                options.Password.RequireDigit = QuizappSettings.Setting().PasswordRequireDigit;
                options.Password.RequireLowercase = QuizappSettings.Setting().PasswordRequireLowerCase;
                options.Password.RequireNonAlphanumeric = QuizappSettings.Setting().PasswordRequireNonAlphaNumeric;
                options.Password.RequireUppercase = QuizappSettings.Setting().PasswordRequireUpperCase;
                options.Password.RequiredLength = QuizappSettings.Setting().PasswordMinimumPasswordLength;
                options.Password.RequiredUniqueChars = QuizappSettings.Setting().PasswordRequireUniqueCharacters;

                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(QuizappSettings.Setting().DefaultLockedTimespan);
                options.Lockout.MaxFailedAccessAttempts = QuizappSettings.Setting().PasswordMaximumFailedAttempts;
                options.Lockout.AllowedForNewUsers = true;

                // User settings.
                options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = false;
            });

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(QuizappSettings.Setting().DefaultSessionTime);

                options.LoginPath = "/Identity/Account/Login";
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.SlidingExpiration = true;
            });
        }

        public static void ProgressiveWebOptions(IServiceCollection services)
        {
            //for progressive web app
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddProgressiveWebApp(new PwaOptions
            {
                OfflineRoute = "/home/offline/"
            });
        }

        public static void CookieOptions(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
        }

        public static void WebOptimizationOptions(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddMvc()
                .AddRazorPagesOptions(options => {
                    //options.Conventions.AddFolderRouteModelConvention("/");
                    options.Conventions.AddPageRoute("/Home/Index", "");
                    options.Conventions.AddPageRoute("/Admin/WithdrawalRequestManagement/{id}", "Admin/WithdrawalRequest/Management/{id}");
                    options.Conventions.AddPageRoute("/Member/Register", "Invitation/{memberCode}");
                    options.Conventions.AddPageRoute("/Member/Register", "Invitation/{memberCode}/{referralId}");
                    //options.Conventions.AddPageRoute("/Member/TransactionStatus", "Member/TransactionStatus/{memberCode}/{resp}");
                    options.Conventions.AuthorizeFolder("/Account/Manage");
                    //options.Conventions.AuthorizeFolder("/Member/");                  
                    options.Conventions.AuthorizePage("/Account/Logout");
                    //options.Conventions.Add(new GlobalPageHandlerModelConvention());
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddSessionStateTempDataProvider();

            services.AddSession();

            // Output caching (https://github.com/madskristensen/WebEssentials.AspNetCore.OutputCaching)
            services.AddOutputCaching(options =>
            {
                options.Profiles["default"] = new OutputCacheProfile
                {
                    Duration = 3600
                };
            });

            // HTML minification (https://github.com/Taritsyn/WebMarkupMin)
            services.AddWebMarkupMin(options =>
            {
                options.AllowMinificationInDevelopmentEnvironment = true;
                options.DisablePoweredByHttpHeaders = true;
            })
                .AddHtmlMinification(options =>
                {
                    options.MinificationSettings.RemoveOptionalEndTags = false;
                    options.MinificationSettings.WhitespaceMinificationMode = WhitespaceMinificationMode.Safe;
                });
            services.AddSingleton<IWmmLogger, WmmNullLogger>(); // Used by HTML minifier

            // Bundling, minification and Sass transpilation (https://github.com/ligershark/WebOptimizer)
            services.AddWebOptimizer(pipeline =>
            {
                pipeline.MinifyJsFiles();
                pipeline.MinifyCssFiles();
                pipeline.MinifyHtmlFiles();
                pipeline.CompileScssFiles()
                        .InlineImages(1);
            });
        }

        public static void Mailers(IServiceCollection services)
        {
            //services.AddTransient<IEmailSender, EmailSender>();
        }

        public static void Authorizations(IServiceCollection services)
        {

        }

        public static void CustomSettings(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<GlobalSettings>(ConnectionManager.Connection);
        }

        public static void Logging(IServiceCollection services)
        {
            services.AddLogging();
        }

        public static void SystemDetections(IServiceCollection services)
        {
            //services.AddDetection();
        }

        public static void RealTime(IServiceCollection services)
        {
            services.AddSignalR();
        }
    }
}
