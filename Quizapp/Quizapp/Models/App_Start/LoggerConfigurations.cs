﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quizapp.Models
{
    public static class LoggerConfigurations
    {
        public static void Set(ILoggerFactory loggerFactory)
        {
            Log4Net(loggerFactory);
        }

        public static void Log4Net(ILoggerFactory loggerFactory)
        {
            //loggerFactory.AddLog4Net();
        }
    }
}
