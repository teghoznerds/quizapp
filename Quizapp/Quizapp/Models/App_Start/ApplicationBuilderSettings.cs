﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
//using Quizapp.Hubs;
//using Hangfire;
using Microsoft.Extensions.Logging;

namespace Quizapp.Models
{
    public static class ApplicationBuilderSettings
    {
        public static void Set(IApplicationBuilder app, QuizappContext db, ILoggerFactory loggerFactory)
        {
            Settings(app, db, loggerFactory);
            QuizappRoutes.SetRoutes(app);
        }

        public static void Settings(IApplicationBuilder app, QuizappContext db, ILoggerFactory loggerFactory)
        {
            Localizations(app, db);
            //app.UseSignalR(routes =>
            //{
            //    routes.MapHub<ChatHub>("/chatHub");
            //});
            //app.UseWebOptimizer();
            //app.UseHttpsRedirection();
            app.UseStaticFiles();            
            //app.UseCookiePolicy();
            app.UseSession();
            app.UseAuthentication();
        }

        public static void Localizations(IApplicationBuilder app, QuizappContext db)
        {
            var supportedCultures = new[]
            {
                    new CultureInfo("en-US"),
                    new CultureInfo("fr-FR"),
            };
            var requestLocalizationOptions = new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("en-US"),

                // Formatting numbers, dates, etc.
                SupportedCultures = supportedCultures,

                // UI strings that we have localized.
                SupportedUICultures = supportedCultures
            };

            app.UseRequestLocalization(requestLocalizationOptions);
        }
    }
}
