﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quizapp.Models
{
    public static class QuizappRoutes
    {
        public static void SetRoutes(IApplicationBuilder app)
        {
            Routes(app);
        }

        public static void Routes(IApplicationBuilder app)
        {
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            //app.UseMvcWithDefaultRoute();
        }
    }
}
