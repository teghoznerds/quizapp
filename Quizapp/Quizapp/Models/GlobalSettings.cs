﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quizapp.Models
{
    public class GlobalSettings
    {
        public GlobalSettings()
        {           
        }
        public string APIBaseUrl { get; set; }

        public void SetEnvironmentVariables(IHostingEnvironment env)
        {
            var apiSettings = ConfigurationManager.AppSetting.GetSection("API");
            if (env.IsProduction())
            {
                APIBaseUrl = apiSettings["Server"];
            }
            else
            {
                APIBaseUrl = apiSettings["Local"];
            }
        }
    }
}
