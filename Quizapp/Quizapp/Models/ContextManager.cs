﻿using Microsoft.EntityFrameworkCore;

namespace Quizapp.Models
{
    public static class ContextManager
    {
        public static QuizappContext GetQuizappContext()
        {
            DbContextOptionsBuilder<QuizappContext> contextOptions = new DbContextOptionsBuilder<QuizappContext>();
            contextOptions.UseSqlServer(ConnectionManager.Connection["ConnectionString"]);           
            return new QuizappContext(contextOptions.Options);
        }
    }
}