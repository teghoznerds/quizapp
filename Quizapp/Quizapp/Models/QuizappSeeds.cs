﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Quizapp.Models
{
    public static class QuizappSeeds
    {
        private static QuizappContext _context;
        private static UserManager<ApplicationUser> _userManager;
        private static RoleManager<IdentityRole> _roleManager;
        private static IHostingEnvironment _hostingEnvironment;

        public static void Initialize(IServiceProvider provider)
        {
            _context = provider.GetRequiredService<QuizappContext>();
            _userManager = provider.GetRequiredService<UserManager<ApplicationUser>>();
            _roleManager = provider.GetRequiredService<RoleManager<IdentityRole>>();
            _hostingEnvironment = provider.GetRequiredService<IHostingEnvironment>();
        }
        public static async Task<IWebHost> QuizAppSeedAsync(this IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                Initialize(services);
                _context.Database.Migrate();             
                await Roles();
                await SeedDefaultAdmin(services);
            }
            return host;
        }

        public static ModelBuilder Temp(this ModelBuilder _modelBuilder)
        {
            return _modelBuilder;
        }

        public static async Task Roles()
        {
            var roles = new string[]{ "AdminSuper", "Admin", "AdminRead", "AdminWrite", "AdminApproval", "School", "SchoolFaculty", "SchoolUser", "Student" };

            foreach(var r in roles)
            {
                bool isExist = await _roleManager.RoleExistsAsync(r);

                if (!isExist)
                {
                    var role = new IdentityRole(r);
                    await _roleManager.CreateAsync(role);
                }
            }

            await Task.CompletedTask;
        }

        public static async Task SeedDefaultAdmin(IServiceProvider provider)
        {
            using (var context = provider.GetRequiredService<QuizappContext>())
            {
                Admin admin = new Admin();
                if (!context.tblAdmin.Any(a => a.Username == "adaasampete@nomail.com"))
                {
                    admin = new Admin
                    {
                        FirstName = "Ada",
                        LastName = "Asampete",
                        FullName = "Ada Asampete",
                        Code = "".GenerateAdminCode(QuizappSettings.Setting(), "Ada Asampete", _context),
                        Email = "adaasampete@nomail.com",
                        PhoneNumber = "+234 813 412 7714",
                        Username = "adaasampete@nomail.com",
                        IsActive = true,
                        DefaultUser = true
                    };

                    context.tblAdmin.Add(admin);
                    context.SaveChanges();
                }
                else
                {
                    admin = context.tblAdmin.Where(a => a.Username == "adaasampete@nomail.com").FirstOrDefault();
                }

                if (context.tblAdmin.Any(a => a.Username == "adaasampete@nomail.com") && !context.Users.Any(a => a.UserName == "adaasampete@nomail.com"))
                {
                    ApplicationUser user = new ApplicationUser
                    {
                        AdminUserId = admin.Id,
                        FirstName = admin.FirstName,
                        LastName = admin.LastName,
                        Email = admin.Email,
                        PhoneNumber = admin.PhoneNumber,
                        UserName = admin.Username,
                        EmailConfirmed = true,
                        PhoneNumberConfirmed = true
                    };

                    var result = await _userManager.CreateAsync(user, "qu!z@Pp18");
                    if (result.Succeeded)
                    {
                        await _userManager.AddToRoleAsync(user, "Admin");
                        await _userManager.AddToRoleAsync(user, "AdminSuper");
                    }
                }
            }
        }
        public static void Users()
        {

        }
        public static ModelBuilder SeedGender(this ModelBuilder _modelBuilder)
        {
            _modelBuilder.Entity<GenderMaster>().HasData(

                new GenderMaster { Gender = "Male", Id = 1 },
                new GenderMaster { Gender = "Female", Id = 2 });

            return _modelBuilder;
        }

        public static async Task SeedCountries(IHostingEnvironment _hostingEnvironment)
        {
            var webRoot = _hostingEnvironment.WebRootPath;
            var query = File.ReadAllText($@"{webRoot}/MigrationScript/countries.sql");

            if ((_context.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).Exists())
            {
                if (Utilities.CheckDatabaseMigrationCompleted(_context))
                {
                    if (_context.tblCountryMaster.ToList().Count() == 0)
                    {
                        await _context.Database.ExecuteSqlCommandAsync(query);
                    }
                }
            }


            await Task.CompletedTask;
        }

        public static ModelBuilder SeedDefaultSchool(this ModelBuilder _modelBuilder)
        {
            _modelBuilder.Entity<School>().HasData(
                new School { Name = "Middlesex University", Id = 1 });
            return _modelBuilder;
        }
    }
}
