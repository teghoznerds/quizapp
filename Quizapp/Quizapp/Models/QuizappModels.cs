﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Quizapp.Models
{
    public class QuizappBase
    {
        public QuizappBase()
        {
            DateCreated = DateTime.Now;
            DateLastUpdated = DateTime.Now;
        }
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastUpdated { get; set; }
    }

    #region Other Tables
    public class Admin : QuizappBase
    {
        public string Email { get; set; }
        public DateTime? DOB { get; set; }
        public string FullName { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Username { get; set; }
        public string Code { get; set; }
        public string DefaultPassword { get; set; }
        public int? CountryId { get; set; }
        public int? StateId { get; set; }
        public int? GenderId { get; set; }
        public bool IsActive { get; set; }
        public bool DefaultUser { get; set; }
        [ForeignKey("StateId")]
        public virtual StateMaster StateMaster { get; set; }
        [ForeignKey("CountryId")]
        public virtual CountryMaster CountryMaster { get; set; }
        [ForeignKey("GenderId")]
        public virtual GenderMaster GenderMaster { get; set; }
    }
    public class Student : QuizappBase
    {
        public string Email { get; set; }
        public DateTime? DOB { get; set; }
        public string FullName { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Username { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }
        public string Code { get; set; }
        public int SchoolId { get; set; }
        public int? CountryId { get; set; }
        public int? StateId { get; set; }
        public int? GenderId { get; set; }
        public int? MemberRefereeId { get; set; }
        public bool IsActive { get; set; }
        public bool IsSchoolRegistered { get; set; }
        public string DefaultPassword { get; set; }
        [ForeignKey("StateId")]
        public virtual StateMaster StateMaster { get; set; }
        [ForeignKey("CountryId")]
        public virtual CountryMaster CountryMaster { get; set; }
        [ForeignKey("GenderId")]
        public virtual GenderMaster GenderMaster { get; set; }
        [ForeignKey("SchoolId")]
        public virtual School School { get; set; }
    }
    public class School: QuizappBase
    {
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string State { get; set; }
        public string Website { get; set; }
        public string Url { get; set; }
        public string Slug { get; set; }
    }
    public class SchoolUsers : QuizappBase
    {
        public string Email { get; set; }
        public DateTime? DOB { get; set; }
        public string FullName { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Username { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }
        public string Code { get; set; }
        public int SchoolId { get; set; }
        public int? CountryId { get; set; }
        public int? StateId { get; set; }
        public int? GenderId { get; set; }
        public int? MemberRefereeId { get; set; }
        public bool IsActive { get; set; }
        public string DefaultPassword { get; set; }
        [ForeignKey("StateId")]
        public virtual StateMaster StateMaster { get; set; }
        [ForeignKey("CountryId")]
        public virtual CountryMaster CountryMaster { get; set; }
        [ForeignKey("GenderId")]
        public virtual GenderMaster GenderMaster { get; set; }
        [ForeignKey("SchoolId")]
        public virtual School School { get; set; }
    }
    public class Faculty: QuizappBase
    {
        public string Email { get; set; }
        public DateTime? DOB { get; set; }
        public string FullName { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Username { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }
        public string Code { get; set; }
        public int SchoolId { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("SchoolId")]
        public virtual School School { get; set; }
    }
    public class FacultyDepartments : QuizappBase
    {
        public int? FacultyId { get; set; }
        public int? DepartmentId { get; set; }
        [ForeignKey("FacultyId")]
        public virtual Faculty Faculty { get; set; }
        [ForeignKey("DepartmentId")]
        public virtual SchoolDepartments Departments { get; set; }
    }
    public class FacultyCourses : QuizappBase
    {
        public int? FacultyId { get; set; }
        public int? CourseId { get; set; }
        [ForeignKey("FacultyId")]
        public virtual Faculty Faculty { get; set; }
        [ForeignKey("CourseId")]
        public virtual ProgrammeCourses ProgrammeCourses { get; set; }
    }
    public class SchoolProgrammeStudents : QuizappBase
    {
        public int? ProgrammeId { get; set; }
        public int? StudentId { get; set; }
        [ForeignKey("StudentId")]
        public virtual Student Student { get; set; }
        [ForeignKey("ProgrammeId")]
        public virtual SchoolProgrammes SchoolProgramme { get; set; }
    }
    public class SchoolProgrammes : QuizappBase
    {
        public int SchoolId { get; set; }
        [ForeignKey("SchoolId")]
        public virtual School School { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int Duration { get; set; }
        public int? ProgramCoordinatorId { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("ProgramCoordinatorId")]
        public virtual SchoolUsers ProgramCoordinator { get; set; }
    }
    public class SchoolDepartments : QuizappBase
    {
        public int SchoolId { get; set; }
        [ForeignKey("SchoolId")]
        public virtual School School { get; set; }
        public string Name { get; set; }
    }
    public class ProgrammeCourses : QuizappBase
    {
        public int ProgramId { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("ProgramId")]
        public virtual SchoolProgrammes SchoolProgrammes { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int Duration { get; set; }
        public int Credit { get; set; }
        public int? CourseCoordinatorId { get; set; }
        [ForeignKey("CourseCoordinatorId")]
        public virtual SchoolUsers ProgramCoordinator { get; set; }
    }
    public class CourseSyllabus : QuizappBase
    {
        public int CourseId { get; set; }
        public int? FacultyId { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("CourseId")]
        public virtual ProgrammeCourses ProgrammeCourses { get; set; }
        [ForeignKey("FacultyId")]
        public virtual Faculty Faculty { get; set; }
        public string Name { get; set; }
    }
    public class CourseSyllabusStudents : QuizappBase
    {
        public int? SyllabusId { get; set; }
        public int? StudentId { get; set; }
        [ForeignKey("StudentId")]
        public virtual Student Student { get; set; }
        [ForeignKey("SyllabusId")]
        public virtual CourseSyllabus CourseSyllabus { get; set; }
    }
    public class SyllabusTopics : QuizappBase
    {
        public int SyllabusId { get; set; }
        [ForeignKey("SyllabusId")]
        public virtual CourseSyllabus CourseSyllabus { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public int Duration { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("ParentId")]
        public virtual SyllabusTopics SyllabusTopicsParent { get; set; }
    }
    public class TopicQuizzes : QuizappBase
    {
        public int SyllabusTopicId { get; set; }
        public bool IsActive { get; set; }
        public double TotalMarkPoint { get; set; }
        public double ExpectedDuration { get; set; }
        [ForeignKey("SyllabusTopicId")]
        public virtual SyllabusTopics SyllabusTopics { get; set; }
        public string Name { get; set; }
    }
    public class QuizzesQuestions : QuizappBase
    {
        public int TopicQuizzesId { get; set; }
        [ForeignKey("TopicQuizzesId")]
        public virtual TopicQuizzes TopicQuizzes { get; set; }
        public string Questions { get; set; }
        public double MarkPoint { get; set; }
        public QuestionTypes AnswerType { get; set; }
        public bool IsActive { get; set; }
    }
    public class QuizzesQuestionChoices : QuizappBase
    {
        public int QuizzesQuestionsId { get; set; }
        public bool IsActive { get; set; }
        public bool IsCorrect { get; set; }
        [ForeignKey("QuizzesQuestionsId")]
        public virtual QuizzesQuestions QuizzesQuestions { get; set; }
        public string Choices { get; set; }
    }
    public class StudentQuiz : QuizappBase
    {
        public int StudentId { get; set; }
        public int QuizId { get; set; }
        public double Result { get; set; }
        public bool IsSubmitted { get; set; }
        [ForeignKey("QuizId")]
        public virtual TopicQuizzes TopicQuizzes { get; set; }
        [ForeignKey("StudentId")]
        public virtual Student Student { get; set; }
    }
    public class StudentQuizAnswers : QuizappBase
    {
        public int StudentQuizId { get; set; }
        public int QuestionId { get; set; }
        public string Answer { get; set; }
        [ForeignKey("QuestionId")]
        public virtual QuizzesQuestions QuizzesQuestions { get; set; }
        [ForeignKey("StudentQuizId")]
        public virtual StudentQuiz StudentQuiz { get; set; }
    }
    public class QuizappSetting : QuizappBase
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    public class SchoolSetting : QuizappBase
    {
        public int SchoolId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        [ForeignKey("SchoolId")]
        public virtual School School { get; set; }
    }
    public class StudentSetting : QuizappBase
    {
        public int StudentId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        [ForeignKey("StudentId")]
        public virtual Student Student { get; set; }
    }
    public class SysteMultimedia : QuizappBase
    {
        public bool ContainsFile { get; set; }
        public string ContentType { get; set; }
        public byte[] FileData { get; set; }
        public string FileUrl { get; set; }
        public string DataImg { get; set; }
        public string FileName { get; set; }
        public int? SchoolId { get; set; }
        public int? AdminId { get; set; }
        public int? SyllabusId { get; set; }
        public int? TopicId { get; set; }
        [ForeignKey("SchoolId")]
        public virtual School School { get; set; }
        [ForeignKey("AdminId")]
        public virtual Admin Admin { get; set; }
        [ForeignKey("SyllabusId")]
        public virtual CourseSyllabus CourseSyllabus { get; set; }
        [ForeignKey("TopicId")]
        public virtual SyllabusTopics SyllabusTopics { get; set; }
    }
    #endregion

    #region Masters
    public class CountryMaster
    {
        public int Id { get; set; }
        public string IsO { get; set; }
        public string CountryName { get; set; }
        public string CountryNiceName { get; set; }
        public string IsO3 { get; set; }
        public string NumCode { get; set; }
        public string PhoneCode { get; set; }
    }
    public class StateMaster : QuizappBase
    {
        public int? CountryId { get; set; }
        public string StateName { get; set; }
        public string StateNiceName { get; set; }
        [ForeignKey("CountryId")]
        public virtual CountryMaster CountryMaster { get; set; }
    }
    public class EmploymentStatusMaster : QuizappBase
    {
        public string Status { get; set; }
        public bool IsActive { get; set; }
    }

    public class GenderMaster : QuizappBase
    {
        public string Gender { get; set; }
        public bool IsActive { get; set; }
    }
    #endregion
}
