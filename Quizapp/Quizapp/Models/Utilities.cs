﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Quizapp.Models;

namespace Quizapp.Models
{
    public static class Utilities
    {
        public static U FetchAndCache<U>(this MemoryCache memorycache, string key, Func<U> retrieveData, MemoryCacheEntryOptions cacheoptions)
        {
            U value;
            if (!memorycache.TryGetValue<U>(key, out value))
            {
                value = retrieveData();

                memorycache.Set(key, value, cacheoptions);
            }
            return value;
        }

        public static string GetCardExpiryYearLastTwoDigits(this int year)
        {
            return year.ToString().Remove(0, 2);
        }


        public static void DisplayTrackedEntities(ChangeTracker changeTracker)
        {
            Console.WriteLine("");

            var entries = changeTracker.Entries();
            foreach (var entry in entries)
            {
                Console.WriteLine("Entity Name: {0}", entry.Entity.GetType().FullName);
                Console.WriteLine("Status: {0}", entry.State);
            }
            Console.WriteLine("");
            Console.WriteLine("---------------------------------------");
        }

        public static string GetInitials(this string fullName)
        {
            string value = "";
            if (!string.IsNullOrEmpty(fullName))
            {
                fullName = fullName = Regex.Replace(fullName, "[^A-Za-z0-9 ]+", "");
                fullName = fullName = Regex.Replace(fullName, @"\s+", " ");

                if (fullName.Contains(' '))
                {
                    fullName.Split(' ').ToList().ForEach(num => { value += (num.Length > 1 ? num.Substring(0, 1) : string.Empty); });
                }
                else
                {
                    fullName.Substring(0, 2);
                }
            }
            else
            {
                value = "UNU";
            }

            return value;
        }

        public static string GetLorem()
        {
            return $@"Lorem ipsum dolor sit amet concealed leaf shah proper council binary. Concealed assured affairs faces, finish easily glows shouted faint. 
Sentence islands spouting we unbeguiled, faces concealed. Diam rays countries, faces fames peeling bind wary catch solomon, painting, they beats evil.
Failing newer landscapes steal retinues vidi rays echoes sheltered evil. Veins concealed spouting obtaining delight wild. Venenatis failing wreaths shouted countries wild, privilege climbing.";
        }

        public static string GenerateStudentCode(this string value, QuizappContext db, AdminSettings settings, string fullName)
        {
            var code = "";
            var lastCount = db.tblStudent.Count() > 0 ? db.tblStudent.Max(c => c.Id) + 1 : 1;
            code = $"{settings.MemberCodePrefix}{GetInitials(fullName).ToUpper()}{lastCount.ToString("0000000")}";
            return code;
        }

        public static string GenerateAdminCode(this string value, AdminSettings settings, string fullName, QuizappContext db = null)
        {
            var context = db == null ? ContextManager.GetQuizappContext() : db;
            var code = "";
            var lastCount = context.tblAdmin.Count() > 0 ? context.tblAdmin.Max(c => c.Id) + 1 : 1;
            code = $"{settings.AdminCodePrefix}{GetInitials(fullName).ToUpper()}{lastCount.ToString("0000000")}";
            return code;
        }

        public static string GenerateRandomPassword(PasswordOptions opts = null)
        {
            if (opts == null) opts = new PasswordOptions()
            {
                RequiredLength = 8,
                RequiredUniqueChars = 4,
                RequireDigit = true,
                RequireLowercase = true,
                RequireNonAlphanumeric = true,
                RequireUppercase = true
            };

            string[] randomChars = new[] {
                "ABCDEFGHJKLMNOPQRSTUVWXYZ",    // uppercase 
                "abcdefghijkmnopqrstuvwxyz",    // lowercase
                "0123456789",                   // digits
                "!@$?_-"                        // non-alphanumeric
            };
            Random rand = new Random(Environment.TickCount);
            List<char> chars = new List<char>();

            if (opts.RequireUppercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[0][rand.Next(0, randomChars[0].Length)]);

            if (opts.RequireLowercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[1][rand.Next(0, randomChars[1].Length)]);

            if (opts.RequireDigit)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[2][rand.Next(0, randomChars[2].Length)]);

            if (opts.RequireNonAlphanumeric)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[3][rand.Next(0, randomChars[3].Length)]);

            for (int i = chars.Count; i < opts.RequiredLength
                || chars.Distinct().Count() < opts.RequiredUniqueChars; i++)
            {
                string rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count),
                    rcs[rand.Next(0, rcs.Length)]);
            }

            return new string(chars.ToArray());
        }
        public static string GenerateBaseUrl(this HttpRequest request, string remaining)
        {
            return $@"{request.Scheme}://{request.Host}/{remaining}";
        }
        public static bool CheckDatabaseMigrationCompleted(QuizappContext db)
        {
            var migrationsAssembly = db.GetService<IMigrationsAssembly>();
            var historyRepository = db.GetService<IHistoryRepository>();

            var all = migrationsAssembly.Migrations.Keys;
            var applied = historyRepository.GetAppliedMigrations().Select(r => r.MigrationId);
            var pending = all.Except(applied);
            var isCompleted = pending.Count() == 0 ? true : false;
            return isCompleted;
        }
        public static string RecursiveMessages(this Exception err)
        {
            var message = err.Message;
            try
            {
                if (err.InnerException != null)
                    message += $"{Environment.NewLine}{RecursiveMessages(err.InnerException)}";
            }
            catch
            {
            }
            return message;
        }
        public static string GetIPAddress1()
        {
            IPHostEntry heserver = Dns.GetHostEntry(Dns.GetHostName());
            var ip = heserver.AddressList[2].ToString();
            return ip;
        }
    }
}
