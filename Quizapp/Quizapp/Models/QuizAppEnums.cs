﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quizapp.Models
{
    public class QuizAppEnums
    {
    }

    public enum QuestionTypes
    {
        SingleObjectives = 0,
        MutipleObjectives = 1,
        ShortAnswer = 2
    }

    public enum ValidationMessageType
    {
        Success,
        Warning,
        Danger,
        Info
    }
}
