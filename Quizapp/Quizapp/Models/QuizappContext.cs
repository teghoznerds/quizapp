﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Quizapp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Quizapp.Models
{
    public class ApplicationUser : IdentityUser
    {
        public int? StudentId { get; set; }
        public int? SchoolId { get; set; }
        public int? SchoolUserId { get; set; }
        public int? AdminUserId { get; set; }
        public string SchoolName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool HasLoggedIn { get; set; }
        [ForeignKey("StudentId")]
        public virtual Student Student { get; set; }
        [ForeignKey("AdminUserId")]
        public virtual Admin Admin { get; set; }
        [ForeignKey("SchoolId")]
        public virtual School School { get; set; }
        [ForeignKey("SchoolUserId")]
        public virtual SchoolUsers SchoolUsers { get; set; }
        [NotMapped]
        public string ConfirmationObject { get; set; }
        [NotMapped]
        public string APIBaseUrl { get; set; }
    }

    public class QuizappContext : IdentityDbContext<ApplicationUser>
    {
        public QuizappContext(DbContextOptions<QuizappContext> options)
        : base(options)
        {
        }

        public DbSet<Student> tblStudent { get; set; }
        public DbSet<Admin> tblAdmin { get; set; }
        public DbSet<QuizappSetting> tblQuizappSetting { get; set; }
        public DbSet<School> tblSchool { get; set; }
        public DbSet<SchoolProgrammes> tblSchoolProgrammes { get; set; }
        public DbSet<ProgrammeCourses> tblProgrammeCourses { get; set; }
        public DbSet<CourseSyllabus> tblCourseSyllabus { get; set; }
        public DbSet<CourseSyllabusStudents> tblCourseSyllabusStudents { get; set; }
        public DbSet<SyllabusTopics> tblSyllabusTopics { get; set; }
        public DbSet<TopicQuizzes> tblTopicQuizzes { get; set; }
        public DbSet<QuizzesQuestions> tblQuizzesQuestions { get; set; }
        public DbSet<QuizzesQuestionChoices> tblQuizzesQuestionChoices { get; set; }
        public DbSet<CountryMaster> tblCountryMaster { get; set; }
        public DbSet<StateMaster> tblStateMaster { get; set; }
        public DbSet<EmploymentStatusMaster> tblEmploymentStatusMaster { get; set; }
        public DbSet<GenderMaster> tblGenderMaster { get; set; }
        public DbSet<Faculty> tblFaculty { get; set; }
        public DbSet<SchoolUsers> tblSchoolUsers { get; set; }
        public DbSet<FacultyDepartments> tblFacultyDepartments { get; set; }
        public DbSet<FacultyCourses> tblFacultyCourses { get; set; }
        public DbSet<SchoolDepartments> tblSchoolDepartments { get; set; }
        public DbSet<StudentSetting> tblStudentSettings { get; set; }
        public DbSet<SchoolSetting> tblSchoolSettings { get; set; }
        public DbSet<SysteMultimedia> tblSysteMultimedia { get; set; }
        public DbSet<SchoolProgrammeStudents> tblSchoolProgrammeStudents { get; set; }
        public DbSet<StudentQuiz> tblStudentQuiz { get; set; }
        public DbSet<StudentQuizAnswers> tblStudentQuizAnswers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            QuizappSeeds.SeedGender(modelBuilder).SeedDefaultSchool();

            modelBuilder.Entity<FacultyCourses>().
                HasOne(pf => pf.Faculty).WithMany().HasForeignKey(p => p.FacultyId).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<StudentQuiz>().
                HasOne(pf => pf.Student).WithMany().HasForeignKey(p => p.StudentId).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<StudentQuizAnswers>().
               HasOne(pf => pf.StudentQuiz).WithMany().HasForeignKey(p => p.StudentQuizId).OnDelete(DeleteBehavior.Restrict);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(ConnectionManager.Connection["ConnectionString"]);
            }
        }
    }
}
