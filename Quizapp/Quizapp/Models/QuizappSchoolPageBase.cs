﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Quizapp.Models
{
    public class QuizappSchoolPageBase : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private QuizappContext _context;
        private readonly ApplicationSettings _applicationSettings;

        public QuizappSchoolPageBase(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            QuizappContext context,
            RoleManager<IdentityRole> roleManager,
            IOptions<ApplicationSettings> options)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _context = context;
            _applicationSettings = options.Value;
        }
        public string ConfirmationObject { get; set; }
        public Quizapp.Models.School School { get; set; }
        public string InvitationLink { get; set; }
        public ApplicationUser UserInfo { get; set; }
        public string APIBaseUrl { get; set; }
        public string BaseUrl { get; set; }
        public string ShareMessage { get; set; }
        [TempData]
        public string MessageStr { get; set; }
        //OnPageHandlerExecuting

        public override async Task OnPageHandlerSelectionAsync(PageHandlerSelectedContext context) {
            APIBaseUrl = _applicationSettings.APIDomain;
            BaseUrl = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
            
            var user = await _userManager.GetUserAsync(User);
            if(user != null)
            {
                School = _context.tblSchool.Where(m => m.Id == user.SchoolId).FirstOrDefault();
                await base.OnPageHandlerSelectionAsync(context);
            }          
        }
    }
}
