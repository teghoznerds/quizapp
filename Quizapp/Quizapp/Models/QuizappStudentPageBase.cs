﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Quizapp.Models
{
    public class QuizappStudentPageBase : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private QuizappContext _context;
        private readonly ApplicationSettings _applicationSettings;

        public QuizappStudentPageBase(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            QuizappContext context,
            RoleManager<IdentityRole> roleManager,
            IOptions<ApplicationSettings> options)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _context = context;
            _applicationSettings = options.Value;
        }
        public string ConfirmationObject { get; set; }
        public Quizapp.Models.Student Student { get; set; }
        public Quizapp.Models.School School { get; set; }
        public string InvitationLink { get; set; }
        public string APIBaseUrl { get; set; }
        public ApplicationUser UserInfo { get; set; }
        public List<SchoolProgrammeStudents> Programs { get; set; }
        public string BaseUrl { get; set; }
        public string ShareMessage { get; set; }
        [TempData]
        public string MessageStr { get; set; }
        //OnPageHandlerExecuting

        public override async Task OnPageHandlerSelectionAsync(PageHandlerSelectedContext context) {
            APIBaseUrl = _applicationSettings.APIDomain;
            BaseUrl = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
            
            var user = await _userManager.GetUserAsync(User);
            if(user != null)
            {
                Student = _context.tblStudent.Include(s => s.School).Where(m => m.Id == user.StudentId).FirstOrDefault();
                School = Student.School;
                Programs = _context.tblSchoolProgrammeStudents.Include(s => s.Student).Include(s => s.SchoolProgramme).Where(s => s.StudentId == Student.Id).ToList();
                await base.OnPageHandlerSelectionAsync(context);
            }          
        }
    }
}
