﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quizapp.Models
{
    public class QuizappViewModels
    {
    }

    public static class ValidationPresenter
    {
        public static string SerializeMessage(this ValidationMessage message)
        {
            return JsonConvert.SerializeObject(message);
        }

        public static ValidationMessage DeSerializeMessage(this string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                return new ValidationMessage();
            }
            return JsonConvert.DeserializeObject<ValidationMessage>(message);
        }
    }

    public class ValidationMessage
    {
        [TempData]
        public string Message { get; set; }
        public ValidationMessageType Type { get; set; }
    }

    public class InputModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}
