﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Quizapp.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Quizapp
{
    public class AuthController : Controller
    {
        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;
        private QuizappContext _context;
        private IHttpContextAccessor _httpContextAccessor;

        public AuthController(UserManager<ApplicationUser> userManager,
                             SignInManager<ApplicationUser> signInManager,
                             QuizappContext context,
                             IHttpContextAccessor httpContextAccessor)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }
        public ActionResult Index()
        {
            var date = DateTime.Now;
            return null;

        }

        public string ABC()
        {
            return "ABC";
        }

        public async Task<IActionResult> Logout(string type)
        {
            try
            {
                if (HttpContext.User.Identity.IsAuthenticated)
                {
                    if (type == "student")
                    {
                        //if (_httpContextAccessor.HttpContext.Session.IsAvailable) { }
                        var currentMember = _userManager.GetUserAsync(User).Result.StudentId;
                        if (currentMember.HasValue)
                        {
                            ApplicationUser appUser = _userManager.GetUserAsync(User).Result;
                            appUser.HasLoggedIn = true;
                            await _userManager.UpdateAsync(appUser);
                            await _signInManager.SignOutAsync();
                            return LocalRedirect("/Student");
                        }
                    }
                    else if (type == "school")
                    {
                        var currentAdmin = _userManager.GetUserAsync(User).Result.SchoolId;
                        if (currentAdmin.HasValue)
                        {
                            ApplicationUser appUser = _userManager.GetUserAsync(User).Result;
                            appUser.HasLoggedIn = true;
                            await _userManager.UpdateAsync(appUser);
                            await _signInManager.SignOutAsync();
                            return LocalRedirect("/School");
                        }
                    }
                    else if (type == "admin")
                    {
                        var currentAdmin = _userManager.GetUserAsync(User).Result.AdminUserId;
                        if (currentAdmin.HasValue)
                        {
                            ApplicationUser appUser = _userManager.GetUserAsync(User).Result;
                            appUser.HasLoggedIn = true;
                            await _userManager.UpdateAsync(appUser);
                            await _signInManager.SignOutAsync();
                            return LocalRedirect("/Admin");
                        }
                    }
                    else
                    {
                        var currentMember = _userManager.GetUserAsync(User).Result.StudentId;
                        if (currentMember.HasValue)
                        {
                            ApplicationUser appUser = _userManager.GetUserAsync(User).Result;
                            appUser.HasLoggedIn = true;
                            await _userManager.UpdateAsync(appUser);
                            await _signInManager.SignOutAsync();
                            return LocalRedirect("/Student");
                        }
                    }
                }
                else
                {
                    return LocalRedirect("/Student");
                }


            }
            catch (Exception e)
            {
                return LocalRedirect("/Student");
            }

            return LocalRedirect("/Student");
        }
    }
}
