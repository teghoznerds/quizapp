﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Quizapp.Models;

namespace Quizapp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private QuizappContext _context;
        private readonly ApplicationSettings _applicationSettings;
        private IHttpContextAccessor _accessor;

        public IndexModel(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            QuizappContext context,
            RoleManager<IdentityRole> roleManager,
            IHttpContextAccessor accessor)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _context = context;
            _accessor = accessor;
        }

        public async Task<IActionResult> OnGetAsync(string schoolslug)
        {
            if(string.IsNullOrEmpty(schoolslug) == false)
            {
                School = _context.tblSchool.Where(s => s.Slug == schoolslug).FirstOrDefault();
            }

            return Page();
        }

        public School School { get; set; }
    }
}
